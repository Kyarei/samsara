//! Context computed from a definition.

use crate::def::*;

use proc_macro::Span as Span1;
use proc_macro2::{Span, TokenStream};
use quote::{format_ident, quote};
use syn::{parse_quote, Generics, Ident, Lifetime, Token, WhereClause, WherePredicate};

use crate::generics;

/// Common type parameters used in generated code.
///
/// # Hygiene
///
/// All of these type parameters are tied to the span of the definition site.
/// This means that they will not conflict with identifiers declared in the
/// macro user’s code.
#[derive(Debug)]
pub struct TypeParams {
    /// Type parameter used to indicate the start state.
    pub s: Ident,
    /// Type parameter used to indicate the end state.
    pub e: Ident,
    /// Type parameter used to indicate another state in methods that extend
    /// a path.
    pub t: Ident,
    /// Type parameter used to indicate a payload type.
    pub p: Ident,
    /// A lifetime parameter.
    pub a_lt: Lifetime,
    /// Another lifetime parameter, when one isn’t enough.
    pub b_lt: Lifetime,
}

impl TypeParams {
    /// Create a new [TypeParams] object.
    pub fn new() -> Self {
        // We must use def_site here instead of mixed_site, as the following
        // identifiers are type parameters or lifetimes that must be resolved
        // at the macro definition site. Unfortunately, def_site is an
        // unstable feature and we write it like this to avoid making users
        // of this macro pass in the procmacro2_semver_exempt config flag.
        let ms_span: Span = Span1::def_site().into();
        let s = format_ident!("S", span = ms_span);
        let e = format_ident!("E", span = ms_span);
        let t = format_ident!("T", span = ms_span);
        let p = format_ident!("P", span = ms_span);
        let a = format_ident!("a", span = ms_span);
        let a_lt = Lifetime {
            apostrophe: ms_span,
            ident: a,
        };
        let b = format_ident!("b", span = ms_span);
        let b_lt = Lifetime {
            apostrophe: ms_span,
            ident: b,
        };
        Self {
            s,
            e,
            t,
            p,
            a_lt,
            b_lt,
        }
    }
}

/// The part of the [FsmContext] that is shared between the forward and reverse
/// FSMs.
#[derive(Debug)]
#[non_exhaustive]
pub struct ModifiedGenerics {
    /// The name of the trait for states.
    pub state_trait: Ident,
    /// `state_trait` with the type parameters from `def.generics` passed in.
    pub state_trait_x: TokenStream,

    /// The generic parameters, with `'a` and `E` added.
    pub end_borrowed_gen: Generics,
    /// The generic parameters, with `'a` and `S` added.
    pub start_borrowed_gen: Generics,
    /// The generic parameters, with `S` added.
    pub start_owned_gen: Generics,
    /// The generic parameters, with `S` and `E` added.
    pub owned_gen: Generics,
    /// The generic parameters, with `'a`, `S`, and `E` added.
    pub borrowed_gen: Generics,

    /// The alignment constraint required to hold an instance of the type
    /// described by `payload_container`.
    pub alignment_constraint: TokenStream,
    /// Describes the type of the buffer used to hold the payloads.
    pub payload_container: TokenStream,
}

impl ModifiedGenerics {
    pub fn new(def: &FsmDefinition, type_params: &TypeParams) -> Self {
        let state_trait = format_ident!("{}State", def.orig_ident);
        let a_lt = &type_params.a_lt;
        let s = &type_params.s;
        let e = &type_params.e;

        let (_geny, genx, _genw) = def.generics.split_for_impl();
        let state_trait_x = quote! {
            #state_trait #genx
        };
        let gen_ae: Generics = parse_quote!(<#a_lt, #e: #state_trait_x>);
        let gen_as: Generics = parse_quote!(<#a_lt, #s: #state_trait_x>);
        let gen_s: Generics = parse_quote!(<#s: #state_trait_x>);
        let gen_se: Generics = parse_quote!(<#s: #state_trait_x, #e: #state_trait_x>);
        let gen_ase: Generics = parse_quote!(<#a_lt, #s: #state_trait_x, #e: #state_trait_x>);

        let end_borrowed_gen = generics::combine(def.generics.clone(), gen_ae);
        let start_borrowed_gen = generics::combine(def.generics.clone(), gen_as);
        let start_owned_gen = generics::combine(def.generics.clone(), gen_s);
        let owned_gen = generics::combine(def.generics.clone(), gen_se);
        let borrowed_gen = generics::combine(def.generics.clone(), gen_ase);

        let genxtf = genx.as_turbofish();
        let base_id = &def.orig_ident;
        let max_align = quote!({#base_id #genxtf::MAX_ALIGN});
        let alignment_constraint = quote!([(); #max_align]:);
        let payload_container = quote! {
            ::samsara::Buffer<#max_align>
        };

        Self {
            state_trait,
            state_trait_x,
            end_borrowed_gen,
            start_borrowed_gen,
            start_owned_gen,
            owned_gen,
            borrowed_gen,
            alignment_constraint,
            payload_container,
        }
    }
}

#[non_exhaustive]
#[derive(Debug)]
/// Context computed from the FSM definition.
pub struct FsmContext<'ctx> {
    /// A reference to the underlying definition.
    pub def: &'ctx FsmDefinition,
    /// A reference to the [TypeParams] object passed into the constructor.
    ///
    /// This is not owned because multiple contexts might be created with the
    /// same [TypeParams] object (such as when the definition calls for
    /// bidirectionality).
    pub type_params: &'ctx TypeParams,
    /// A vector of length `def.len()` such that `transitions_beginning_at[i]`
    /// contains all transitions that start at state `i`.
    pub transitions_beginning_at: Vec<Vec<usize>>,
    /// A vector of length `def.len()` such that `transitions_ending_at[i]`
    /// contains all transitions that end at state `i`.
    pub transitions_ending_at: Vec<Vec<usize>>,

    /// The part of the context that is shared between the forward and
    /// reverse FSMs.
    ///
    /// This is not owned because multiple contexts might be created with the
    /// same [TypeParams] object (such as when the definition calls for
    /// bidirectionality).
    pub modified_generics: &'ctx ModifiedGenerics,

    /// The name of the trait for payloads.
    pub payload_trait: Ident,
    /// The name of the type used for borrowed paths.
    pub slice_type: Ident,
    /// The name of the type used for mutable borrowed paths.
    pub slice_type_mut: Ident,
    /// The name of the type used for owned paths.
    pub vec_type: Ident,
    /// The directional state trait. When the definition is set to not be
    /// bidirectional, this is the same as the nondirectional state trait.
    pub state_trait_d: Ident,
    /// `state_trait_d` with the type parameters from `def.generics` passed in.
    pub state_trait_dx: TokenStream,
    /// The lifetime bounds that must be placed on the associated types
    /// `SplitFirst`, `SplitFirstMut`, `SplitLast`, and `SplitLastMut` in
    /// the state trait.
    pub reference_lifetime_bounds: WhereClause,
}

impl<'ctx> FsmContext<'ctx> {
    pub fn new(
        def: &'ctx FsmDefinition,
        type_params: &'ctx TypeParams,
        modified_generics: &'ctx ModifiedGenerics,
    ) -> Self {
        let (transitions_beginning_at, transitions_ending_at) =
            def.beginning_and_ending_transitions();

        let payload_trait = format_ident!("{}Payload", def.ident);
        let slice_type = format_ident!("{}Path", def.ident);
        let slice_type_mut = format_ident!("{}PathMut", def.ident);
        let vec_type = format_ident!("{}PathBuf", def.ident);

        let state_trait_d = if def.direction == Direction::Rev || !def.settings.bidirectional {
            format_ident!("{}State", def.ident)
        } else {
            format_ident!("{}FwdState", def.ident)
        };
        let genx = def.generics.split_for_impl().1;
        let state_trait_dx = quote! {
            #state_trait_d #genx
        };

        let a_lt = &type_params.a_lt;
        let reference_lifetime_bounds = WhereClause {
            where_token: Token![where](Span::call_site()),
            predicates: Iterator::chain(
                def.generics.type_params().map::<WherePredicate, _>(|tp| {
                    let id = &tp.ident;
                    parse_quote! { #id: #a_lt }
                }),
                def.generics.lifetimes().map::<WherePredicate, _>(|lt| {
                    parse_quote! { #lt: #a_lt }
                }),
            )
            .collect(),
        };

        Self {
            def,
            type_params,
            transitions_beginning_at,
            transitions_ending_at,
            modified_generics,
            payload_trait,
            slice_type,
            slice_type_mut,
            vec_type,
            state_trait_d,
            state_trait_dx,
            reference_lifetime_bounds,
        }
    }
}
