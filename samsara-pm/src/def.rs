//! Types for defining input for the [crate::samsara!] macro.

use std::{collections::HashMap, mem};

use bitflags::bitflags;
use proc_macro2::TokenStream;
use quote::{format_ident, quote};
use syn::{
    parse::{Parse, ParseStream},
    punctuated::Punctuated,
    Attribute, Generics, Ident, Token, Type, Visibility, WhereClause,
};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Direction {
    Fwd,
    Rev,
}

impl Direction {
    pub fn suffix(&self) -> &'static str {
        match self {
            Direction::Fwd => "",
            Direction::Rev => "Rev",
        }
    }
}

impl Default for Direction {
    fn default() -> Self {
        Direction::Fwd
    }
}

/// An item found after the equal signs in an FSM definition.
enum FsmDefItem {
    /// A state declaration.
    State(Vec<Attribute>, Ident),
    /// A transition declaration.
    Transition(
        Vec<Attribute>,
        Ident,
        Token![->],
        Box<Type>,
        Token![->],
        Ident,
    ),
}

impl Parse for FsmDefItem {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let attrs = input.call(Attribute::parse_outer)?;
        let ident: Ident = input.parse()?;
        Ok(if input.lookahead1().peek(Token![->]) {
            FsmDefItem::Transition(
                attrs,
                ident,
                input.parse()?,
                input.parse()?,
                input.parse()?,
                input.parse()?,
            )
        } else {
            FsmDefItem::State(attrs, ident)
        })
    }
}

bitflags! {
    /// Controls which traits to derive for the path types.
    #[derive(Default)]
    pub struct DeriveFlags : u8 {
        const CLONE = 0x01;
        const DEBUG = 0x02;
        const PARTIAL_EQ = 0x04;
        const EQ = 0x08;
    }
}

/// Settings controlled by attributes on the definition.
#[derive(Debug, Default, Clone, Copy)]
#[non_exhaustive]
pub struct FsmSettings {
    /// Controls which traits to derive for the path types.
    pub derive_flags: DeriveFlags,
    /// If true, generate types for the reversed finite state machine, as well
    /// as methods to reverse path types.
    pub bidirectional: bool,
}

/// A state of a finite state machine.
#[derive(Debug, Clone)]
pub struct FsmState {
    /// Any attributes attached to the state declaration.
    pub attrs: Vec<Attribute>,
    /// The name of the state.
    pub name: Ident,
}

/// A transition of a finite state machine.
#[derive(Debug, Clone)]
pub struct FsmTransition {
    /// Any attributes attached to the transition declaration.
    pub attrs: Vec<Attribute>,
    /// The index of the start state in [FsmDefinition::states].
    pub start: usize,
    /// The type of the payload associated with the transition.
    pub payload: Type,
    /// The index of the end state in [FsmDefinition::states].
    pub end: usize,
}

/// Parsed form of input to [crate::samsara!].
#[derive(Debug, Clone)]
#[non_exhaustive]
pub struct FsmDefinition {
    /// Settings controlled by attributes on the definition.
    pub settings: FsmSettings,
    /// Attributes not specific to samsara.
    pub attrs: Vec<Attribute>,
    /// The visibility of the definition.
    pub visibility: Visibility,
    /// The identifier used for the finite state machine.
    pub ident: Ident,
    /// The identifier used for the original finite state machine.
    ///
    /// Same as `ident` if this is not a [reverse](FsmDefinition::reversed)
    /// of another FSM; otherwise, it is the `ident` of that FSM.
    pub orig_ident: Ident,
    /// Whether the FSM is the forward variant or the reverse.
    pub direction: Direction,
    /// Any generic arguments for the finite state machine.
    pub generics: Generics,
    /// The states of the finite state machine.
    pub states: Vec<FsmState>,
    /// The transitions of the finite state machine.
    pub transitions: Vec<FsmTransition>,
}

impl Parse for FsmDefinition {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let mut settings: FsmSettings = Default::default();
        let mut attrs = Vec::new();
        for attr in input.call(Attribute::parse_outer)?.into_iter() {
            if attr.path.is_ident("samsara_derive") {
                for trait_name in
                    attr.parse_args_with(Punctuated::<Ident, Token![,]>::parse_terminated)?
                {
                    match &trait_name.to_string()[..] {
                        "Clone" => settings.derive_flags |= DeriveFlags::CLONE,
                        "Debug" => settings.derive_flags |= DeriveFlags::DEBUG,
                        "PartialEq" => settings.derive_flags |= DeriveFlags::PARTIAL_EQ,
                        "Eq" => settings.derive_flags |= DeriveFlags::EQ,
                        name => {
                            return Err(syn::Error::new(
                                trait_name.span(),
                                format!("Deriving {} is not supported", name),
                            ))
                        }
                    }
                }
            } else if attr.path.is_ident("samsara") {
                for trait_name in
                    attr.parse_args_with(Punctuated::<Ident, Token![,]>::parse_terminated)?
                {
                    match &trait_name.to_string()[..] {
                        "bidirectional" => settings.bidirectional = true,
                        name => {
                            return Err(syn::Error::new(
                                trait_name.span(),
                                format!("Unknown directive {}", name),
                            ))
                        }
                    }
                }
            } else {
                attrs.push(attr)
            }
        }
        let visibility = input.parse()?;
        let ident: Ident = input.parse()?;
        let mut generics: Generics = input.parse()?;
        let where_clause = input.parse::<WhereClause>().ok();
        generics.where_clause = where_clause;
        let _: Token![=] = input.parse()?;
        let items: Punctuated<FsmDefItem, Token![,]> = input.parse_terminated(FsmDefItem::parse)?;
        let mut state_ids_by_name: HashMap<Ident, usize> = HashMap::new();
        let mut states = vec![];
        let mut transitions = vec![];
        for item in items {
            match item {
                FsmDefItem::State(attrs, name) => {
                    state_ids_by_name.insert(name.clone(), states.len());
                    states.push(FsmState { attrs, name });
                }
                FsmDefItem::Transition(attrs, start, _, payload, _, end) => {
                    let start = *state_ids_by_name
                        .get(&start)
                        .ok_or_else(|| syn::Error::new_spanned(start, "state not found"))?;
                    let end = *state_ids_by_name
                        .get(&end)
                        .ok_or_else(|| syn::Error::new_spanned(end, "state not found"))?;
                    transitions.push(FsmTransition {
                        attrs,
                        start,
                        payload: *payload,
                        end,
                    });
                }
            }
        }
        Ok(FsmDefinition {
            settings,
            attrs,
            visibility,
            orig_ident: ident.clone(),
            direction: Direction::Fwd,
            ident,
            generics,
            states,
            transitions,
        })
    }
}

impl FsmDefinition {
    /// Returns the list of transitions beginning and ending at each state.
    ///
    /// This function returns a pair `(transitions_beginning_at, transitions_ending_at)`,
    /// both having the same length as the number of states, such that
    /// `transitions_beginning_at[i]` contains all transitions that start at
    /// state `i` and `transitions_ending_at[i]` contains all transitions that
    /// end at state `i`. Both of these lists actually contain indices into
    /// [FsmDefinition::transitions].
    pub fn beginning_and_ending_transitions(&self) -> (Vec<Vec<usize>>, Vec<Vec<usize>>) {
        let n = self.states.len();
        // Analyze the FSM
        let mut transitions_beginning_at: Vec<Vec<usize>> = vec![vec![]; n];
        let mut transitions_ending_at: Vec<Vec<usize>> = vec![vec![]; n];
        for (i, FsmTransition { start, end, .. }) in self.transitions.iter().enumerate() {
            transitions_beginning_at[*start].push(i);
            transitions_ending_at[*end].push(i);
        }
        (transitions_beginning_at, transitions_ending_at)
    }

    /// Returns a reversed version of a finite state machine.
    ///
    /// [FsmDefinition::ident] is changed, but [FsmDefinition::orig_ident]
    /// is not.
    pub fn reversed(mut self) -> Self {
        for transition in self.transitions.iter_mut() {
            mem::swap(&mut transition.start, &mut transition.end);
        }
        self.ident = format_ident!("{}Rev", self.ident);
        self.direction = Direction::Rev;
        self
    }

    /// Returns the [::std::marker::PhantomData] fields needed inside a struct
    /// definition to mark the struct as owning any of the payloads.
    pub fn generate_phantom_decls_for_owned_type<'a>(
        &'a self,
    ) -> impl Iterator<Item = TokenStream> + 'a {
        self.transitions.iter().enumerate().map(|(i, trans)| {
            let field = format_ident!("phantom_{}", i);
            let payload = &trans.payload;
            quote! {
                #field: ::std::marker::PhantomData<#payload>
            }
        })
    }

    pub fn generate_phantom_fields(&self) -> impl Iterator<Item = TokenStream> {
        (0..self.transitions.len()).map(|i| {
            let field = format_ident!("phantom_{}", i);
            quote! {
                #field: ::std::marker::PhantomData
            }
        })
    }
}
