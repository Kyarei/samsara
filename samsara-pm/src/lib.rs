//! Proc-macro crate for samsara.
//!
//! This documentation is meant for the unfortunate souls who have to maintain this crate.
#![feature(proc_macro_def_site)]

// We use pub(crate) here for now, since we can’t publicly export these items from a proc-macro crate.
// The items in question will be made public *if* they get moved to their own crate.
pub(crate) mod actx;
pub(crate) mod ctx;
pub(crate) mod def;
pub(crate) mod expand;
pub(crate) mod generics;
pub(crate) mod make_path;

use actx::*;
use ctx::*;
use def::*;
use expand::addc::*;
use make_path::PathDef;

use generics::{Subst, Subst2, SubstN};
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use quote::{format_ident, quote};
use syn::{parse_macro_input, parse_quote, Generics, Ident};
use syn_path::path;

impl FsmDefinition {
    pub(crate) fn expand(&self) -> TokenStream2 {
        // Sanity checks
        if self.transitions.len() > (u8::MAX as usize) {
            return quote! { compile_error!("Finite state machines with more than {} transitions are currently not supported (you have {} transitions)", u8::MAX, self.transitions.len()) };
        }

        let type_params = TypeParams::new();
        let modified_generics = ModifiedGenerics::new(self, &type_params);
        if self.settings.bidirectional {
            let rev = self.clone().reversed();
            let primal = self.expand_inner(&type_params, &modified_generics, Some(&rev.ident));
            let dual = rev.expand_inner(&type_params, &modified_generics, Some(&self.ident));
            let vis = &self.visibility;
            let (geny, genx, genw) = self.generics.split_for_impl();
            let s = &type_params.s;
            let gen_s: Generics = parse_quote!(<#s>);
            let mut gen_ps = generics::combine(self.generics.clone(), gen_s);
            let gen_ps_w = gen_ps.make_where_clause();
            let state_trait = format_ident!("{}State", self.ident);
            let fwd = format_ident!("{}FwdState", self.ident);
            let rev = format_ident!("{}RevState", self.ident);
            gen_ps_w
                .predicates
                .push(parse_quote! { #s: #fwd #genx + #rev #genx });
            let (gen_ps_y, _, gen_ps_w) = gen_ps.split_for_impl();
            quote! {
                #primal
                #dual
                #vis trait #state_trait #geny : #fwd #genx + #rev #genx #genw {}
                impl #gen_ps_y #state_trait #genx for #s #gen_ps_w {}
            }
        } else {
            self.expand_inner(&type_params, &modified_generics, None)
        }
    }

    fn expand_inner(
        &self,
        type_params: &TypeParams,
        modified_generics: &ModifiedGenerics,
        reverse_id: Option<&Ident>,
    ) -> TokenStream2 {
        let ctx = FsmContext::new(self, type_params, modified_generics);

        let reversed = self.direction == Direction::Rev;

        let state_trait = &ctx.modified_generics.state_trait;
        let slice_type = &ctx.slice_type;
        let slice_type_mut = &ctx.slice_type_mut;
        let vec_type = &ctx.vec_type;
        let state_trait_d = &ctx.state_trait_d;
        let TypeParams { s, e, t, .. } = ctx.type_params;

        // Generics objects
        let generics = &self.generics;
        let (_, genx, _) = generics.split_for_impl();
        let state_trait_x = quote! {
            #state_trait #genx
        };
        let state_trait_dx = quote! {
            #state_trait_d #genx
        };

        let owned_gen = &ctx.modified_generics.owned_gen;
        let (owned_gen_y, owned_gen_x, owned_gen_w) = owned_gen.split_for_impl();
        let owned_gen_x_t2 = Subst(&owned_gen_x, e, t);
        let owned_gen_x_xet = Subst2(&owned_gen_x, s, e, e, s);

        let borrowed_gen = &ctx.modified_generics.borrowed_gen;
        let (borrowed_gen_y, borrowed_gen_x, borrowed_gen_w) = borrowed_gen.split_for_impl();

        let alignment_constraint = &modified_generics.alignment_constraint;
        let owned_gen_w_with_align_constraint =
            add_alignment_constraint(owned_gen_w, alignment_constraint);
        let borrowed_gen_w_with_align_constraint =
            add_alignment_constraint(borrowed_gen_w, alignment_constraint);
        let payload_container = &modified_generics.payload_container;

        let derive_flags = self.settings.derive_flags;

        let owned_gen_w_with_align_and_clone_constraints = add_trait_constraints(
            generics,
            &owned_gen_w_with_align_constraint,
            &path!(::std::clone::Clone),
        );

        let debug_contents = derive_flags.contains(DeriveFlags::DEBUG).then(|| {
            let owned_gen_w_with_align_and_debug_constraints = add_trait_constraints(
                generics,
                &owned_gen_w_with_align_constraint,
                &path!(::std::fmt::Debug),
            );
            let borrowed_gen_w_with_align_and_debug_constraints = add_trait_constraints(
                generics,
                &borrowed_gen_w_with_align_constraint,
                &path!(::std::fmt::Debug),
            );
            let slice_type_str = slice_type.to_string();
            let slice_type_mut_str = slice_type_mut.to_string();
            let vec_type_str = vec_type.to_string();

            let outer_clauses = self.states.iter().enumerate().map(|(i, state)| {
                let state_name = &state.name;
                let (clauses, ind_action) = ctx.start_items(
                    i,
                    debug_action,
                );
                quote! {
                    #i => {
                        let should_pop_discriminant = <#state_name #genx as #state_trait_dx>::NEEDS_DISCRIMINANT_FROM;
                        if should_pop_discriminant {
                            let discriminant;
                            (discriminant, discriminants) = unsafe {
                                discriminants.split_first().unwrap_unchecked()
                            };
                            match discriminant {
                                #clauses
                                _ => unsafe { ::std::hint::unreachable_unchecked() },
                            }
                        } else {
                            #ind_action
                        }
                    }
                }
            });

            quote! {
                impl #borrowed_gen_y #slice_type #borrowed_gen_x #borrowed_gen_w_with_align_and_debug_constraints {
                    fn fmt_base(&self, f: &mut ::std::fmt::Formatter<'_>) -> Result<(), ::std::fmt::Error> {
                        f.write_str(" { ")?;
                        let mut id = <#s as #state_trait_dx>::ID;
                        let mut discriminants = self.discriminants;
                        let mut payloads = self.payloads;
                        while !discriminants.is_empty() || !payloads.is_empty() {
                            match id {
                                #(#outer_clauses,)*
                                _ => unsafe { ::std::hint::unreachable_unchecked() },
                            }
                        }
                        debug_assert!(id == <#e as #state_trait_dx>::ID);
                        f.write_str(<#e as #state_trait_dx>::NAME)?;
                        f.write_str(" }")
                    }
                }

                impl #borrowed_gen_y ::std::fmt::Debug for #slice_type #borrowed_gen_x #borrowed_gen_w_with_align_and_debug_constraints {
                    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> Result<(), ::std::fmt::Error> {
                        f.write_str(#slice_type_str)?;
                        self.fmt_base(f)
                    }
                }
                impl #borrowed_gen_y ::std::fmt::Debug for #slice_type_mut #borrowed_gen_x #borrowed_gen_w_with_align_and_debug_constraints {
                    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> Result<(), ::std::fmt::Error> {
                        f.write_str(#slice_type_mut_str)?;
                        #slice_type::from(self).fmt_base(f)
                    }
                }
                impl #owned_gen_y ::std::fmt::Debug for #vec_type #owned_gen_x #owned_gen_w_with_align_and_debug_constraints {
                    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> Result<(), ::std::fmt::Error> {
                        f.write_str(#vec_type_str)?;
                        self.view().fmt_base(f)
                    }
                }
            }
        });

        let partial_eq_contents = derive_flags.contains(DeriveFlags::PARTIAL_EQ).then(|| {
            let owned_gen_w_with_align_and_peq_constraints = add_trait_constraints(
                generics,
                &owned_gen_w_with_align_constraint,
                &path!(::std::cmp::PartialEq),
            );
            let borrowed_gen_w_with_align_and_peq_constraints = add_trait_constraints(
                generics,
                &borrowed_gen_w_with_align_constraint,
                &path!(::std::cmp::PartialEq),
            );

            let outer_clauses = self.states.iter().enumerate().map(|(i, state)| {
                let state_name = &state.name;
                let (clauses, ind_action) = ctx.start_items(
                    i,
                    partial_eq_action,
                );
                quote! {
                    #i => {
                        let should_pop_discriminant = <#state_name #genx as #state_trait_dx>::NEEDS_DISCRIMINANT_FROM;
                        if should_pop_discriminant {
                            let discriminant;
                            let discriminant2;
                            (discriminant, discriminants) = unsafe {
                                discriminants.split_first().unwrap_unchecked()
                            };
                            (discriminant2, discriminants2) = unsafe {
                                discriminants2.split_first().unwrap_unchecked()
                            };
                            if discriminant != discriminant2 {
                                return false;
                            }
                            match discriminant {
                                #clauses
                                _ => unsafe { ::std::hint::unreachable_unchecked() },
                            }
                        } else {
                            #ind_action
                        }
                    }
                }
            });

            quote! {
                impl #borrowed_gen_y ::std::cmp::PartialEq for #slice_type #borrowed_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &Self) -> bool {
                        let mut id = <#s as #state_trait_dx>::ID;
                        let mut discriminants = self.discriminants;
                        let mut payloads = self.payloads;
                        let mut discriminants2 = other.discriminants;
                        let mut payloads2 = other.payloads;
                        while !discriminants.is_empty() || !payloads.is_empty() {
                            if discriminants2.is_empty() && payloads2.is_empty() {
                                return false;
                            }
                            match id {
                                #(#outer_clauses,)*
                                _ => unsafe { ::std::hint::unreachable_unchecked() },
                            }
                        }
                        debug_assert!(id == <#e as #state_trait_dx>::ID);
                        discriminants2.is_empty() && payloads2.is_empty()
                    }
                }
                impl #borrowed_gen_y ::std::cmp::PartialEq<#slice_type_mut #borrowed_gen_x> for #slice_type #borrowed_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &#slice_type_mut #borrowed_gen_x) -> bool {
                        *self == #slice_type::from(other)
                    }
                }
                impl #borrowed_gen_y ::std::cmp::PartialEq<#vec_type #owned_gen_x> for #slice_type #borrowed_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &#vec_type #owned_gen_x) -> bool {
                        *self == other.view()
                    }
                }
                impl #borrowed_gen_y ::std::cmp::PartialEq<#slice_type #borrowed_gen_x> for #slice_type_mut #borrowed_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &#slice_type #borrowed_gen_x) -> bool {
                        #slice_type::from(self) == *other
                    }
                }
                impl #borrowed_gen_y ::std::cmp::PartialEq for #slice_type_mut #borrowed_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &Self) -> bool {
                        #slice_type::from(self) == #slice_type::from(other)
                    }
                }
                impl #borrowed_gen_y ::std::cmp::PartialEq<#vec_type #owned_gen_x> for #slice_type_mut #borrowed_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &#vec_type #owned_gen_x) -> bool {
                        #slice_type::from(self) == other.view()
                    }
                }
                impl #borrowed_gen_y ::std::cmp::PartialEq<#slice_type #borrowed_gen_x> for #vec_type #owned_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &#slice_type #borrowed_gen_x) -> bool {
                        self.view() == *other
                    }
                }
                impl #borrowed_gen_y ::std::cmp::PartialEq<#slice_type_mut #borrowed_gen_x> for #vec_type #owned_gen_x #borrowed_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &#slice_type_mut #borrowed_gen_x) -> bool {
                        self.view() == #slice_type::from(other)
                    }
                }
                impl #owned_gen_y ::std::cmp::PartialEq for #vec_type #owned_gen_x #owned_gen_w_with_align_and_peq_constraints {
                    fn eq(&self, other: &Self) -> bool {
                        self.view() == other.view()
                    }
                }
            }
        });

        let reverse_contents = reverse_id.map(|reverse_id| {
            let state_trait_rev = if reversed {
                format_ident!("{}FwdState", reverse_id)
            } else {
                format_ident!("{}State", reverse_id)
            };
            let state_trait_rev_x = quote! {
                #state_trait_rev #genx
            };
            let slice_type_rev = format_ident!("{}Path", reverse_id);
            let slice_type_mut_rev = format_ident!("{}PathMut", reverse_id);
            let vec_type_rev = format_ident!("{}PathBuf", reverse_id);
            let extend_reversed_raw_method = |clone| {
                let method_name = if clone {
                    format_ident!("extend_reversed_from_slice_raw")
                } else {
                    format_ident!("extend_reversed_raw")
                };
                let outer_clauses = self.states.iter().enumerate().map(|(i, state)| {
                    let state_name = &state.name;
                    let (clauses, ind_action) = start_items(
                        &ctx.transitions_beginning_at[i],
                        state_name,
                        &genx,
                        &owned_gen_x,
                        &state_trait_rev_x,
                        self,
                        |ctx| extend_reversed_raw_action(ctx, &vec_type, e, clone),
                    );
                    quote! {
                        #i => {
                            let should_pop_discriminant = <#state_name #genx as #state_trait_rev_x>::NEEDS_DISCRIMINANT_TO;
                            if should_pop_discriminant {
                                let discriminant;
                                (discriminant, discriminants_src) = unsafe {
                                    discriminants_src.split_last().unwrap_unchecked()
                                };
                                match discriminant {
                                    #clauses
                                    _ => unsafe { ::std::hint::unreachable_unchecked() },
                                }
                            } else {
                                #ind_action
                            }
                        }
                    }
                });
                quote! {
                    unsafe fn #method_name(
                        discriminants_dst: &mut ::std::vec::Vec<u8>,
                        payloads_dst: &mut #payload_container,
                        mut discriminants_src: &[u8],
                        mut payloads_src: &[::std::mem::MaybeUninit<u8>],
                        // start and end states are wrt destination buffers
                        // i.e. the starting state for the source is state_end
                        state_start: usize,
                        state_end: usize,
                    ) {
                        let mut id = state_start;
                        while !discriminants_src.is_empty() || !payloads_src.is_empty() {
                            match id {
                                #(#outer_clauses,)*
                                _ => unsafe { ::std::hint::unreachable_unchecked() },
                            }
                        }
                        debug_assert!(id == state_end);
                    }
                }
            };
            let owned_gen_x_t1 = Subst(&owned_gen_x, s, t);
            let extend_reversed_raw = extend_reversed_raw_method(false);
            let clone_contents = derive_flags.contains(DeriveFlags::CLONE).then(|| {
                let extend_reversed_from_slice_raw = extend_reversed_raw_method(true);
                quote! {
                    impl #owned_gen_y #vec_type #owned_gen_x #owned_gen_w_with_align_and_clone_constraints {
                        #extend_reversed_from_slice_raw

                        pub fn extended_reversed_from_slice<#t>(self, after: &#slice_type_rev #owned_gen_x_t1) -> #vec_type #owned_gen_x_t2
                        where
                            #t: #state_trait_x,
                            #alignment_constraint,
                        {
                            let (mut discriminants_dst, mut payloads_dst) = self.into_parts();

                            unsafe {
                                Self::extend_reversed_from_slice_raw(
                                    &mut discriminants_dst, &mut payloads_dst,
                                    after.discriminants, after.payloads,
                                    <#e as #state_trait_dx>::ID, <#t as #state_trait_dx>::ID);
                                #vec_type::from_parts(discriminants_dst, payloads_dst)
                            }
                        }
                        pub fn extended_reversed_from_slice_mut<#t>(self, after: &#slice_type_mut_rev #owned_gen_x_t1) -> #vec_type #owned_gen_x_t2
                        where
                            #t: #state_trait_x,
                            #alignment_constraint,
                        {
                            self.extended_reversed_from_slice(&#slice_type_rev::from(after))
                        }

                        pub fn reversed_from_slice(slice: &#slice_type_rev #owned_gen_x_xet) -> Self {
                            #vec_type::new().extended_reversed_from_slice(slice)
                        }
                        pub fn reversed_from_slice_mut(slice: &#slice_type_mut_rev #owned_gen_x_xet) -> Self {
                            #vec_type::new().extended_reversed_from_slice_mut(slice)
                        }
                    }
                }
            });
            quote! {
                impl #owned_gen_y #vec_type #owned_gen_x #owned_gen_w_with_align_constraint {
                    #extend_reversed_raw
                    pub fn extended_reversed<#t>(self, after: #vec_type_rev #owned_gen_x_t1) -> #vec_type #owned_gen_x_t2
                    where
                        #t: #state_trait_x,
                        #alignment_constraint,
                    {
                        let (mut discriminants_dst, mut payloads_dst) = self.into_parts();
                        let (discriminants_src, payloads_src) = after.into_parts();

                        unsafe {
                            Self::extend_reversed_raw(
                                &mut discriminants_dst, &mut payloads_dst,
                                &discriminants_src, &payloads_src,
                                <#e as #state_trait_dx>::ID, <#t as #state_trait_dx>::ID);
                            <#vec_type #owned_gen_x_t2>::from_parts(discriminants_dst, payloads_dst)
                        }
                    }
                    pub fn reversed(self) -> #vec_type_rev #owned_gen_x_xet
                    where #alignment_constraint {
                        #vec_type_rev::new().extended_reversed(self)
                    }
                }
                #clone_contents
            }
        });

        let front_matter = expand::frontmatter::expand_front_matter(&ctx);

        let state_trait_def = expand::state::declare_state_trait(&ctx);
        let state_trait_impls = self
            .states
            .iter()
            .enumerate()
            .map(|(i, state)| expand::state::implement_state_trait(i, state, &ctx));

        let payload_trait_def = expand::payload::declare_payload_trait(&ctx);
        let payload_trait_impls =
            self.transitions.iter().enumerate().map(|(i, transition)| {
                expand::payload::implement_payload_trait(i, transition, &ctx)
            });

        let struct_defs = expand::structs::expand_definitions(&ctx);

        let immutable_slice_impl = expand::structs::expand_immutable_slice_impl(&ctx);
        let mutable_slice_impl = expand::structs::expand_mutable_slice_impl(&ctx);
        let vec_impl = expand::structs::expand_vec_impl(&ctx);

        let slicing_conversions = expand::structs::from::expand_slicing_conversions(&ctx);

        let drop_impl = expand::structs::drop::impl_drop(&ctx);
        let clone_impl = expand::structs::clone::impl_clone(&ctx);

        quote! {
            #front_matter

            // State types
            #state_trait_def
            #(#state_trait_impls)*

            // Transition types
            // for insertion:
            #payload_trait_def
            #(#payload_trait_impls)*

            #struct_defs

            #immutable_slice_impl
            #mutable_slice_impl
            #vec_impl

            #slicing_conversions

            #drop_impl
            #clone_impl
            #debug_contents
            #partial_eq_contents
            #reverse_contents
        }
    }
}

fn extend_reversed_raw_action(
    ctx: ActionContextStart,
    vec_type: &Ident,
    e: &Ident,
    clone: bool,
) -> TokenStream2 {
    let ActionContextStart {
        payload,
        cur_state,
        next_state,
        next_state_id: end_id,
        typarams,
        full_typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    let payload_expr = if clone {
        quote! {
            Clone::clone(&*payload_ptr)
        }
    } else {
        quote! {
            ::std::ptr::read(payload_ptr)
        }
    };
    let full_typarams = full_typarams.as_turbofish();
    let next_vec_type_params = SubstN(&full_typarams, e, quote! { #cur_state #typarams });
    quote!({
        let payload_ptr;
        (payload_ptr, discriminants_src, payloads_src) = unsafe {
            <#cur_state #typarams as #state_trait_x>::v0tbump_slices::<#next_state #tf, #payload>(discriminants_src, payloads_src)
        };
        let payload = unsafe { #payload_expr };
        #vec_type #next_vec_type_params::push_raw::<#next_state #typarams, #payload>(discriminants_dst, payloads_dst, payload);
        id = #end_id;
    })
}

fn debug_action(ctx: ActionContextStart) -> TokenStream2 {
    let ActionContextStart {
        payload,
        cur_state,
        next_state,
        next_state_id: end_id,
        typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    quote!({
        let payload_ptr;
        (payload_ptr, discriminants, payloads) = unsafe {
            <#cur_state #typarams as #state_trait_x>::bump_slices::<#next_state #tf, #payload>(discriminants, payloads)
        };
        let payload = unsafe { &*payload_ptr };
        f.write_str(<#cur_state #typarams as #state_trait_x>::NAME)?;
        f.write_str(" => ")?;
        <#payload as ::std::fmt::Debug>::fmt(payload, f)?;
        f.write_str(" => ")?;
        id = #end_id;
    })
}

fn partial_eq_action(ctx: ActionContextStart) -> TokenStream2 {
    let ActionContextStart {
        payload,
        cur_state,
        next_state,
        next_state_id: end_id,
        typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    quote!({
        let payload_ptr;
        (payload_ptr, discriminants, payloads) = unsafe {
            <#cur_state #typarams as #state_trait_x>::bump_slices::<#next_state #tf, #payload>(discriminants, payloads)
        };
        let payload_ptr2;
        (payload_ptr2, discriminants2, payloads2) = unsafe {
            <#cur_state #typarams as #state_trait_x>::bump_slices::<#next_state #tf, #payload>(discriminants2, payloads2)
        };
        let payload = unsafe { &*payload_ptr };
        let payload2 = unsafe { &*payload_ptr2 };
        if payload != payload2 {
            return false;
        }
        id = #end_id;
    })
}

/// See `::samsara::samsara`.
#[proc_macro]
pub fn samsara(item: TokenStream) -> TokenStream {
    let def = parse_macro_input!(item as FsmDefinition);
    def.expand().into()
}

/// See `::samsara::make_path`.
#[proc_macro]
pub fn make_path(item: TokenStream) -> TokenStream {
    let def = parse_macro_input!(item as PathDef);
    def.expand().into()
}

#[cfg(test)]
mod tests {
    use proc_macro2::Span;
    use quote::ToTokens;
    use syn::{
        punctuated::Punctuated, token::Pub, ItemStruct, Path, PathArguments, PathSegment, Type,
        TypePath, VisPublic, Visibility,
    };

    use crate::*;

    #[test]
    #[ignore = "currently uses Span::def_site() from proc_macro because it is unstable and thus semver-exempt in proc_macro2"]
    fn test_expand() {
        let span = Span::call_site();
        let mut the_i32: Punctuated<syn::PathSegment, syn::token::Colon2> = Punctuated::new();
        the_i32.push_value(PathSegment {
            ident: Ident::new("i32", span),
            arguments: PathArguments::None,
        });
        let def = FsmDefinition {
            settings: Default::default(),
            attrs: vec![],
            visibility: Visibility::Public(VisPublic {
                pub_token: Pub { span },
            }),
            ident: Ident::new("ZeroOne", span),
            orig_ident: Ident::new("ZeroOne", span),
            generics: Generics {
                lt_token: None,
                params: Punctuated::new(),
                gt_token: None,
                where_clause: None,
            },
            direction: Direction::Fwd,
            states: vec![
                FsmState {
                    attrs: vec![],
                    name: Ident::new("Zero", span),
                },
                FsmState {
                    attrs: vec![],
                    name: Ident::new("One", span),
                },
                FsmState {
                    attrs: vec![],
                    name: Ident::new("Two", span),
                },
            ],
            transitions: vec![
                FsmTransition {
                    attrs: vec![],
                    start: 0,
                    payload: Type::Path(TypePath {
                        qself: None,
                        path: Path {
                            leading_colon: None,
                            segments: the_i32.clone(),
                        },
                    }),
                    end: 1,
                },
                FsmTransition {
                    attrs: vec![],
                    start: 1,
                    payload: Type::Path(TypePath {
                        qself: None,
                        path: Path {
                            leading_colon: None,
                            segments: the_i32.clone(),
                        },
                    }),
                    end: 0,
                },
                FsmTransition {
                    attrs: vec![],
                    start: 1,
                    payload: Type::Path(TypePath {
                        qself: None,
                        path: Path {
                            leading_colon: None,
                            segments: the_i32.clone(),
                        },
                    }),
                    end: 2,
                },
            ],
        };
        def.expand();
    }

    #[test]
    #[ignore = "currently uses Span::def_site() from proc_macro because it is unstable and thus semver-exempt in proc_macro2"]
    fn test_expand_complicated() {
        let input = r#"
            pub Creature =
            Sleeping,
            Sitting,
            Eating,
            Dead,
            Sleeping -> Alarm -> Sitting,
            Sitting -> Meal -> Eating,
            Eating -> Time -> Sitting,
            Sitting -> () -> Sleeping,
            Sitting -> Death -> Dead,
            Sleeping -> Death -> Dead,
            "#;

        let stream: TokenStream2 = input.parse().expect("Failed to parse into tokens");

        let def = syn::parse2::<FsmDefinition>(stream).expect("Failed to parse definition");
        let _expanded = def.expand();
    }

    #[test]
    fn test_subst_self() {
        let c = format_ident!("C");
        let z = format_ident!("Z");
        let input = quote! { struct Foo<A, B, C, D>; };
        let parsed: ItemStruct = syn::parse2(input).unwrap();
        let subst = Subst(&parsed, &c, &z);
        let expected = quote! { struct Foo<A, B, Z, D>; };
        // Not so robust, but this is one of the least difficult ways to test
        // output to a TokenStream.
        assert_eq!(subst.to_token_stream().to_string(), expected.to_string());
    }
}
