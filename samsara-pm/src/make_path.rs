//! Types for the [crate::make_path!] macro.

use proc_macro2::TokenStream as TokenStream2;
use quote::quote;
use syn::{
    parse::{Parse, ParseStream},
    parse_quote, AngleBracketedGenericArguments, Expr, GenericArgument, Ident, Token,
};

/// A transition from the previous state, used in [PathDef].
#[derive(Debug)]
pub struct PathTrans {
    /// The first arrow token.
    _arrow1: Token![=>],
    /// The payload of this transition.
    payload: Expr,
    /// The second arrow token.
    _arrow2: Token![=>],
    /// The state to move to.
    next: Ident,
}

impl Parse for PathTrans {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(PathTrans {
            _arrow1: input.parse()?,
            payload: input.parse()?,
            _arrow2: input.parse()?,
            next: input.parse()?,
        })
    }
}

/// Parsed form of input to [crate::make_path!].
#[derive(Debug)]
pub struct PathDef {
    /// The name of the owned-path type.
    fsm_type: Ident,
    /// The generic arguments specified after the type, if any.
    generics: Option<AngleBracketedGenericArguments>,
    /// The colon.
    _colon: Token![:],
    /// The starting state.
    start_state: Ident,
    /// A list of transitions from the starting state.
    transitions: Vec<PathTrans>,
}

impl Parse for PathDef {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let mut def = PathDef {
            fsm_type: input.parse()?,
            generics: if input.peek(Token![<]) {
                Some(input.parse()?)
            } else {
                None
            },
            _colon: input.parse()?,
            start_state: input.parse()?,
            transitions: Vec::new(),
        };
        loop {
            match input.parse::<PathTrans>() {
                Ok(o) => def.transitions.push(o),
                Err(_) => return Ok(def),
            }
        }
    }
}

impl PathDef {
    pub fn expand(&self) -> TokenStream2 {
        let fsm_type = &self.fsm_type;
        let start_state = &self.start_state;
        let s: GenericArgument = match &self.generics {
            None => parse_quote! {#start_state},
            Some(generics) => {
                let g = generics.args.iter();
                parse_quote! { #start_state<#(#g),*> }
            }
        };
        let sstf = match &self.generics {
            None => quote! { <#start_state, #start_state> },
            Some(generics) => {
                let mut elems = Vec::new();
                for e in &generics.args {
                    match e {
                        GenericArgument::Const(_) => (),
                        _ => elems.push(e),
                    }
                }
                elems.push(&s);
                elems.push(&s);
                for e in &generics.args {
                    match e {
                        GenericArgument::Const(_) => elems.push(e),
                        _ => (),
                    }
                }
                quote! {
                    <#(#elems),*>
                }
            }
        };
        let qu = quote!('_);
        let u = quote!(_);
        let skeletonized_generics = self.generics.as_ref().map(|a| {
            a.args
                .iter()
                .map(|g| match g {
                    GenericArgument::Lifetime(_) => &qu,
                    _ => &u,
                })
                .collect::<Vec<_>>()
        });
        let skeletonized_generics = match skeletonized_generics {
            None => quote!(),
            Some(g) => quote!(<#(#g),*>),
        };
        let pushes = self.transitions.iter().map(|trans| {
            let next = &trans.next;
            let payload = &trans.payload;
            let trtf = quote! {
                <#next #skeletonized_generics, _>
            };
            quote! {
                .pushed::#trtf(#payload)
            }
        });
        quote! {
            #fsm_type::#sstf::new()
                #(#pushes)*
        }
    }
}
