//! Functions to expand sections of code for the [crate::samsara!] macro.
//!
//! # Data layout
//!
//! **TODO:** Summarize the implications on layout of being able to destructure
//! from both ends in constant time.
//!
//! The types generated from the macro use a low-level layout to represent
//! paths. They use two buffers of data: the *discriminant buffer* and the
//! *payload buffer*. The discriminant buffer holds the discriminants that
//! determine which transition is taken from or to the current state, as well
//! as information about padding bytes inserted.
//!
//! Each transition corresponds to an entry in the discriminant buffer, an
//! entry in the payload buffer, or both:
//!
//! * A discriminant must be stored for the transition to take when the
//!   source state has more than one departing transition, or if
//!   the destination state has more than one arriving transition.
//! * If the payload is zero-sized, then the discriminant must be stored
//!   regardless of whether the prior condition holds.
//! * For a transition with a source `S` and destination `E`, the
//!   discriminant must be stored if `S` has any outgoing transitions
//!   that require storing the discriminant, or if `E` has any incoming
//!   transitions that require storing the discriminant. This strategy
//!   allows for efficient destructuring from both ends.
//!
//! Since the entry for the discriminant is only one byte large, there is a
//! hard limit of 256 states for the finite state machine. It is possible in
//! principle to relax this limitation to 256 transitions from or to each
//! state, but such a change is not planned to be implemented.
//!
//! In principle, it is possible to elide the discriminant for a transition
//! *from* a state with no *predecessors* or one *to* a state with no
//! *successors*; however, this is not currently done.
//!
//! Payloads must always be aligned according to their own alignment.
//! Padding will be added between payloads in order to satisfy this requirement.
//! In order to allow for efficient destructuring from both ends, the amount
//! of padding might need to be stored in the discriminant buffer. Specifically,
//! given a transition from state `S` to state `E` with a payload of type `P`:
//!
//! * Let `MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD` be the minimum alignment of the
//!   payloads of all transitions with `S` as the *destination*.
//! * Let `MAX_PAYLOAD_ALIGN` be the maximum alignment of the payloads of all
//!   transitions with `E` as the *source*.
//! * If `MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD >= MAX_PAYLOAD_ALIGN`, then the
//!   amount of padding used is guaranteed to be zero; therefore, it does
//!   not need to be stored in the discriminant buffer.
//! * Otherwise, the amount of padding needs to be stored but is guaranteed
//!   to be divisible by `MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD`. The generated
//!   code thus stores the number of padding bytes divided by
//!   `MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD`.
//!
//! For the ‘owned’ path type, the payload buffer is a [`Vec<u8>`](Vec) with an
//! allocator that always allocates at the required alignment. For ‘borrowed’
//! path types, the payload buffer is a slice whose first byte is aligned to
//! the payload of the first transition.
//!
//! If both the padding and discriminant bytes are stored for a transition,
//! then the padding byte is stored *before* the discriminant byte.
//!
//! # Naming
//!
//! In the documentation for the items in this module, we use the following
//! convention for identifiers produced by expanded code:
//!
//! * `<id>` refers to the name given by the user in the [crate::samsara!]
//!   macro, immediately before the generic parameters (if any) and the equal
//!   sign.
//! * `<state>` refers to the name of a state when context makes it clear.
//!
//! In type signatures, we often omit `<id>` in type names. For example, if
//! we want to refer to the type `<id>PathBuf`, we might refer to it simply
//! as `PathBuf`.

pub mod addc;
pub mod extend;
pub mod frontmatter;
pub mod payload;
pub mod structs;
pub mod state;
