//! Definitions of path structures and operations on them.

pub mod clone;
pub mod drop;
pub mod from;

use proc_macro2::TokenStream;
use quote::{format_ident, quote};

use crate::{
    ctx::FsmContext,
    generics::{Subst, Subst2},
};

use super::{addc::add_alignment_constraint, extend::expand_extend_raw};

/// Expand to the definitions of the path structures.
///
/// The [crate::samsara!] macro generates three structures from an FSM definition:
///
/// * `<id>Path`: an immutable view of a path.
/// * `<id>PathMut`: a mutable view of a path. Payloads may be mutated, but
///   no transitions may be inserted, removed, or altered to a transition of
///   another type.
/// * `<id>PathBuf`: an owned path. Modification at the end is allowed.
///
/// Due to limitations of the Rust language, `<id>Path` and `<id>PathMut`
/// themselves act as references; that is, they correspond to `&[T]` and to
/// `&mut [T]` rather than to `[T]`. Unfortunately, this slightly hinders the
/// ergonomics of using these types.
pub fn expand_definitions(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let vis = &def.visibility;
    let slice_type = &ctx.slice_type;
    let slice_type_mut = &ctx.slice_type_mut;
    let vec_type = &ctx.vec_type;
    let payload_container = &ctx.modified_generics.payload_container;
    let a_lt = &ctx.type_params.a_lt;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;

    let (borrowed_gen_y, _, borrowed_gen_w) = ctx.modified_generics.borrowed_gen.split_for_impl();
    let (owned_gen_y, _, owned_gen_w) = ctx.modified_generics.owned_gen.split_for_impl();
    let alignment_constraint = &ctx.modified_generics.alignment_constraint;
    let owned_gen_w_with_align_constraint =
        add_alignment_constraint(owned_gen_w, alignment_constraint);

    let slice_type_phantom_decls = def.transitions.iter().enumerate().map(|(i, trans)| {
        let field = format_ident!("phantom_{}", i);
        let payload = &trans.payload;
        quote! {
            #field: ::std::marker::PhantomData<&#a_lt #payload>
        }
    });
    let slice_type_mut_phantom_decls = def.transitions.iter().enumerate().map(|(i, trans)| {
        let field = format_ident!("phantom_{}", i);
        let payload = &trans.payload;
        quote! {
            #field: ::std::marker::PhantomData<&#a_lt mut #payload>
        }
    });
    let vec_type_phantom_decls = def.generate_phantom_decls_for_owned_type();

    quote! {
        #[derive(Copy, Clone)]
        #vis struct #slice_type #borrowed_gen_y #borrowed_gen_w {
            // FIXME: right now, #slice_type is the reference type, not
            // &#slice_type.
            discriminants: &#a_lt [u8],
            payloads: &#a_lt [::std::mem::MaybeUninit<u8>],
            phantom: ::std::marker::PhantomData<(#s, #e)>,
            // This struct also acts like it can borrow any of the payloads.
            // Declare that here so the drop check works correctly.
            #(#slice_type_phantom_decls,)*
        }
        #vis struct #slice_type_mut #borrowed_gen_y #borrowed_gen_w {
            // FIXME: right now, #slice_type_mut is the reference type, not
            // &mut #slice_type.
            discriminants: &#a_lt mut [u8],
            payloads: &#a_lt mut [::std::mem::MaybeUninit<u8>],
            phantom: ::std::marker::PhantomData<(#s, #e)>,
            // This struct also acts like it can mutably borrow any of the
            // payloads. Declare that here so the drop check works correctly.
            #(#slice_type_mut_phantom_decls,)*
        }
        #vis struct #vec_type #owned_gen_y #owned_gen_w_with_align_constraint {
            discriminants: ::std::vec::Vec<u8>,
            // FIXME: this might store padding bits!
            payloads: #payload_container,
            phantom: ::std::marker::PhantomData<(#s, #e)>,
            // This struct also acts like it can own any of the payloads.
            // Declare that here so the drop check works correctly.
            #(#vec_type_phantom_decls,)*
        }
    }
}

/// Expand to the inherent implementation of `<id>Path`.
pub fn expand_immutable_slice_impl(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let slice_type = &ctx.slice_type;
    let state_trait_dx = &ctx.state_trait_dx;
    let a_lt = &ctx.type_params.a_lt;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;

    let (borrowed_gen_y, borrowed_gen_x, borrowed_gen_w) =
        ctx.modified_generics.borrowed_gen.split_for_impl();

    let vec_type_phantom_uses = def.generate_phantom_fields();

    quote! {
        impl #borrowed_gen_y #slice_type #borrowed_gen_x #borrowed_gen_w {
            pub const unsafe fn from_raw_parts(discriminants: &#a_lt [u8], payloads: &#a_lt [::std::mem::MaybeUninit<u8>]) -> Self {
                Self {
                    discriminants,
                    payloads,
                    phantom: ::std::marker::PhantomData,
                    #(#vec_type_phantom_uses,)*
                }
            }

            pub const fn discriminants(&self) -> &[u8] {
                return self.discriminants;
            }

            pub const fn payloads(&self) -> &[::std::mem::MaybeUninit<u8>] {
                return self.payloads;
            }

            pub const fn is_empty(&self) -> bool {
                self.discriminants.is_empty() && self.payloads.is_empty()
            }

            pub fn split_first(&self) -> Option<<#s as #state_trait_dx>::SplitFirst<'_, #e>> {
                unsafe {
                    <#s as #state_trait_dx>::split_first(self.discriminants, self.payloads)
                }
            }

            pub fn split_last(&self) -> Option<<#e as #state_trait_dx>::SplitLast<'_, #s>> {
                unsafe {
                    <#e as #state_trait_dx>::split_last(self.discriminants, self.payloads)
                }
            }
        }
    }
}

/// Expand to the inherent implementation of `<id>PathMut`.
pub fn expand_mutable_slice_impl(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let slice_type_mut = &ctx.slice_type_mut;
    let state_trait_dx = &ctx.state_trait_dx;
    let a_lt = &ctx.type_params.a_lt;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;

    let (borrowed_gen_y, borrowed_gen_x, borrowed_gen_w) =
        ctx.modified_generics.borrowed_gen.split_for_impl();

    let vec_type_phantom_uses = def.generate_phantom_fields();

    quote! {
        impl #borrowed_gen_y #slice_type_mut #borrowed_gen_x #borrowed_gen_w {
            // not yet const-able: no mut references in const
            pub unsafe fn from_raw_parts(discriminants: &#a_lt mut [u8], payloads: &#a_lt mut [::std::mem::MaybeUninit<u8>]) -> Self {
                Self {
                    discriminants,
                    payloads,
                    phantom: ::std::marker::PhantomData,
                    #(#vec_type_phantom_uses,)*
                }
            }

            pub const fn discriminants(&self) -> &[u8] {
                return self.discriminants;
            }

            pub const fn payloads(&self) -> &[::std::mem::MaybeUninit<u8>] {
                return self.payloads;
            }

            pub const fn is_empty(&#a_lt self) -> bool {
                self.discriminants.is_empty() && self.payloads.is_empty()
            }

            pub fn split_first(&self) -> Option<<#s as #state_trait_dx>::SplitFirst<'_, #e>> {
                unsafe {
                    <#s as #state_trait_dx>::split_first(self.discriminants, self.payloads)
                }
            }

            pub fn split_first_mut(&mut self) -> Option<<#s as #state_trait_dx>::SplitFirstMut<'_, #e>> {
                unsafe {
                    <#s as #state_trait_dx>::split_first_mut(self.discriminants, self.payloads)
                }
            }

            pub fn split_last(&self) -> Option<<#e as #state_trait_dx>::SplitLast<'_, #s>> {
                unsafe {
                    <#e as #state_trait_dx>::split_last(self.discriminants, self.payloads)
                }
            }

            pub fn split_last_mut(&mut self) -> Option<<#e as #state_trait_dx>::SplitLastMut<'_, #s>> {
                unsafe {
                    <#e as #state_trait_dx>::split_last_mut(self.discriminants, self.payloads)
                }
            }
        }
    }
}

/// Expand to the inherent implementation of `<id>PathBuf`.
pub fn expand_vec_impl(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let slice_type = &ctx.slice_type;
    let slice_type_mut = &ctx.slice_type_mut;
    let vec_type = &ctx.vec_type;
    let state_trait_dx = &ctx.state_trait_dx;
    let payload_container = &ctx.modified_generics.payload_container;
    let payload_trait = &ctx.payload_trait;
    let state_trait_x = &ctx.modified_generics.state_trait_x;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;
    let t = &ctx.type_params.t;
    let p = &ctx.type_params.p;

    let (to_tp_py, _, to_tp_pw) = ctx.modified_generics.start_owned_gen.split_for_impl();
    let (owned_gen_y, owned_gen_x, owned_gen_w) = ctx.modified_generics.owned_gen.split_for_impl();
    let alignment_constraint = &ctx.modified_generics.alignment_constraint;
    let to_tp_pw_with_align_constraint = add_alignment_constraint(to_tp_pw, alignment_constraint);
    let owned_gen_w_with_align_constraint =
        add_alignment_constraint(owned_gen_w, alignment_constraint);

    let owned_gen_x_t2 = Subst(&owned_gen_x, e, t);
    let owned_gen_x_shet = Subst2(&owned_gen_x, s, e, e, t);
    let owned_gen_x_s2 = Subst(&owned_gen_x, e, s);

    let vec_type_phantom_uses: Vec<_> = def.generate_phantom_fields().collect();

    let extend_raw = expand_extend_raw(false, &ctx);

    quote! {
        // Primitive / raw methods – private and usually unsafe
        impl #owned_gen_y #vec_type #owned_gen_x #owned_gen_w_with_align_constraint {
            // not yet const-able: no deref coercion or ptr::read in const
            pub fn into_parts(self) -> (::std::vec::Vec<u8>, #payload_container) {
                let this = ::std::mem::ManuallyDrop::new(self);
                unsafe {
                    (
                        ::std::ptr::read(::std::ptr::addr_of!(this.discriminants)),
                        ::std::ptr::read(::std::ptr::addr_of!(this.payloads)),
                    )
                }
                // we have nothing else to drop
            }

            pub const unsafe fn from_parts(
                discriminants: ::std::vec::Vec<u8>,
                payloads: #payload_container) -> Self {
                #vec_type {
                    discriminants,
                    payloads,
                    phantom: ::std::marker::PhantomData,
                    #(#vec_type_phantom_uses,)*
                }
            }

            // not yet const-able: no const trait impls
            unsafe fn push_raw<#t, #p>(
                discriminants: &mut ::std::vec::Vec<u8>,
                payloads: &mut #payload_container,
                payload: #p)
            where
                #t: #state_trait_x,
                #p: #payload_trait #owned_gen_x_shet,
            {
                // eprintln!("Before pushing:");
                // dbg!(&discriminants);
                // dbg!(&payloads);
                let payload_size = ::std::mem::size_of::<#p>();
                let payload_align = ::std::mem::align_of::<#p>();
                let payload_offset = ::samsara::next_multiple_of(payloads.len(), payload_align);
                let padding = payload_offset - payloads.len();
                assert!(payload_offset < (isize::MAX as usize));

                // Do we need to store the amount of padding?
                // If the payload alignment is less than or equal to the
                // minimum possible alignment of the last payload, then there
                // are no padding bits necessary.
                let min_align_after_previous_payload = <#e as #state_trait_dx>::MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD;
                let must_store_padding = <#t as #state_trait_dx>::MAX_PAYLOAD_ALIGN > min_align_after_previous_payload;
                if must_store_padding {
                    // eprintln!("Padding stored");
                    debug_assert!(padding % min_align_after_previous_payload == 0);
                    discriminants.push(
                        u8::try_from(padding / min_align_after_previous_payload)
                            .unwrap_or_else(|_| panic!(
                                "Need more than {} padding bits! I give up!",
                                (u8::MAX as usize) * min_align_after_previous_payload)));
                } else {
                    // eprintln!("Padding not stored");
                }
                // Do we need a discriminant byte?
                let needs_discriminant = <#e as #state_trait_dx>::NEEDS_DISCRIMINANT_FROM || <#t as #state_trait_dx>::NEEDS_DISCRIMINANT_TO;
                let discriminant = <#p as #payload_trait #owned_gen_x_shet>::DISCRIMINANT;
                if needs_discriminant {
                    // eprintln!("Using discriminant");
                    discriminants.push(discriminant);
                } else {
                    // eprintln!("Not using discriminant");
                }
                // Now push the payload.
                // Pad the vector to the alignment if necessary.
                // Also give room for the payload itself.
                payloads.resize(payload_offset + payload_size, ::std::mem::MaybeUninit::uninit());
                unsafe {
                    ::std::ptr::write(payloads.as_mut_ptr().offset(payload_offset as isize).cast::<#p>(), payload);
                }
                // eprintln!("After pushing:");
                // dbg!(&discriminants);
                // dbg!(&payloads);
            }

            #extend_raw

            // public methods – wrappers around raw methods,
            // as well as trivial things

            pub fn pushed<#t, #p>(self, payload: #p) -> #vec_type #owned_gen_x_t2
            where
                #t: #state_trait_x,
                #p: #payload_trait #owned_gen_x_shet,
            {
                let (mut discriminants, mut payloads) = self.into_parts();

                unsafe {
                    Self::push_raw(&mut discriminants, &mut payloads, payload);
                    #vec_type::from_parts(discriminants, payloads)
                }
            }

            pub fn extended<#t>(self, after: #vec_type #owned_gen_x_shet) -> #vec_type #owned_gen_x_t2
            where
                #t: #state_trait_x
            {
                let (mut discriminants_dst, mut payloads_dst) = self.into_parts();
                let (discriminants_src, payloads_src) = after.into_parts();

                unsafe {
                    Self::extend_raw(
                        &mut discriminants_dst, &mut payloads_dst,
                        &discriminants_src, &payloads_src,
                        <#e as #state_trait_dx>::ID, <#t as #state_trait_dx>::ID);
                    #vec_type::from_parts(discriminants_dst, payloads_dst)
                }
            }

            // not yet const-able: Vec::is_empty is not const
            pub fn is_empty(&self) -> bool {
                self.payloads.is_empty() && self.discriminants.is_empty()
            }

            pub fn popped(self) -> Result<<#e as #state_trait_dx>::Pop<#s>, Self> where #alignment_constraint {
                <#e as #state_trait_dx>::popped(self)
            }

            // not yet const-able: no deref coercion in const
            pub fn view(&self) -> #slice_type #owned_gen_x {
                self.into()
            }
            pub fn view_mut(&mut self) -> #slice_type_mut #owned_gen_x {
                self.into()
            }
        }
        impl #to_tp_py #vec_type #owned_gen_x_s2 #to_tp_pw_with_align_constraint {
            pub const fn new() -> Self {
                #vec_type {
                    discriminants: ::std::vec::Vec::new(),
                    payloads: ::samsara::create_buffer(),
                    phantom: ::std::marker::PhantomData,
                    #(#vec_type_phantom_uses,)*
                }
            }
        }
    }
}
