//! Functions for expanding the state trait definition and implementations.

use proc_macro2::TokenStream;
use quote::{format_ident, quote};
use syn::Ident;

use crate::{
    actx::{ActionContextEnd, ActionContextStart},
    ctx::FsmContext,
    def::{Direction, FsmState, FsmTransition},
    expand::addc::add_alignment_constraint,
    generics::{Subst, Subst2, SubstN},
};

/// Expand to the definition for the trait for states in the finite state machine.
///
/// # Items
///
/// This trait is named `<id>State` (abbreviated as `State` hereafter) and
/// contains the following items:
///
/// ## Associated types for destructuring
///
/// * `SplitFirst<'a, E: State>`: the result of splitting a path into its first
///   transition and the remainder. For a state `<state>` implementing this
///   trait, this is an enum named `<state>SplitFirst`. For reverse paths, this
///   is instead named `<state>SplitFirstRev`. This enum has one variant for
///   each transition possible from the start state.
/// * `SplitFirstMut<'a, E: State>`: like `SplitFirst`, but with mutable access.
/// * `SplitLast<'a, S: State>` and `SplitLastMut<'a, S: State>`: the result
///   of splitting a path into its last transition and everything before it.
/// * `Popped<S>`: the result of *removing* the last transition from a path.
///
/// ## Associated constants
///
/// * `NEEDS_DISCRIMINANT_FROM`: true if this state needs a discriminant to
///   determine which transition to take next.
/// * `NEEDS_DISCRIMINANT_TO`: true if this state needs a discriminant to
///   determine which transition preceded it.
/// * `MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD`: the minimum alignment guaranteed
///   to immediately follow the previous payload when this state is reached.
/// * `MAX_PAYLOAD_ALIGN`: the maximum alignment required for the payload of
///   a following transition.
/// * `ID`: the numerical identifier of this state.
/// * `NAME`: the name of this state as a string.
///
/// ## High-level destructuring methods
///
/// For brevity, most of the type bounds have been omitted from the method signatures.
///
/// * `popped<S>(v: PathBuf<Self, S>) -> Result<Self::Pop<S>, PathBuf<Self, S>`:
///   removes the last transition from an owned path. Used for `PathBuf::popped`.
/// * `unsafe split_first<'a, E>(discriminants: &'a [u8], payloads: &'a [u8]) -> Option<Self::SplitFirst<'a, E>>`:
///   used for `Path::split_first`. Unsafe because this method doesn’t check
///   that the contents of `discriminants` or `payloads` are valid.
/// * `unsafe split_first_mut<'a, E>(discriminants: &'a mut [u8], payloads: &'a mut [u8]) -> Option<Self::SplitFirstMut<'a, E>>`
/// * `unsafe split_last<'a, S>(discriminants: &'a [u8], payloads: &'a [u8]) -> Option<Self::SplitLast<'a, S>>`
/// * `unsafe split_last_mut<'a, S>(discriminants: &'a mut [u8], payloads: &'a mut [u8]) -> Option<Self::SplitLastMut<'a, S>>`
///
/// ## Low-level destructuring methods
///
/// * `unsafe fn bump_slices_raw<E, P<Self, E>>(discriminants: &[u8], payloads: &[MaybeUninit<u8>]) -> (*const P, usize, usize)`:
///   Low-level method for iterating forwards through a path. You probably want
///   one of the following two methods:
///   * `unsafe bump_slices<'a, 'b, E, P<Self, E>>(dicriminants: &'a [u8], payloads: &'b [MaybeUninit<u8>]) -> (*const P, &'a [u8], &'b [MaybeUninit<u8>])`
///   * `unsafe bump_slices_mut<'a, 'b, E, P<Self, E>>(dicriminants: &'a mut [u8], payloads: &'b mut [MaybeUninit<u8>]) -> (*mut P, &'a mut [u8], &'b mut [MaybeUninit<u8>])`
/// * `unsafe fn v0tbump_slices_raw<S, P<S, Self>>(discriminants: &[u8], payloads: &[MaybeUninit<u8>]) -> (*const P, usize, usize)`:
///   Low-level method for iterating backwards through a path. You probably want
///   one of the following two methods:
///   * `unsafe v0tbump_slices<'a, 'b, S, P<S, Self>>(dicriminants: &'a [u8], payloads: &'b [MaybeUninit<u8>]) -> (*const P, &'a [u8], &'b [MaybeUninit<u8>])`
///   * `unsafe v0tbump_slices_mut<'a, 'b, S, P<S, Self>>(dicriminants: &'a mut [u8], payloads: &'b mut [MaybeUninit<u8>]) -> (*mut P, &'a mut [u8], &'b mut [MaybeUninit<u8>])`
pub fn declare_state_trait(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let vis = &def.visibility;
    let a_lt = &ctx.type_params.a_lt;
    let b_lt = &ctx.type_params.b_lt;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;
    let p = &ctx.type_params.p;
    let vec_type = &ctx.vec_type;
    let reference_lifetime_bounds = &ctx.reference_lifetime_bounds;
    let alignment_constraint = &ctx.modified_generics.alignment_constraint;

    let generics = &def.generics;
    let (geny, _, genw) = generics.split_for_impl();
    let owned_gen = &ctx.modified_generics.owned_gen;
    let (_, owned_gen_x, _) = owned_gen.split_for_impl();

    let selfid = format_ident!("Self");
    let payload_trait = &ctx.payload_trait;

    let state_trait_d = &ctx.state_trait_d;
    let state_trait_x = &ctx.modified_generics.state_trait_x;
    let state_trait_dx = &ctx.state_trait_dx;
    let owned_gen_x_self1 = Subst(&owned_gen_x, s, &selfid);
    let owned_gen_x_self2 = Subst(&owned_gen_x, e, &selfid);

    quote! {
        #vis unsafe trait #state_trait_d #geny: Sized #genw {
            type SplitFirst<#a_lt, #e: #state_trait_x> #reference_lifetime_bounds;
            type SplitFirstMut<#a_lt, #e: #state_trait_x> #reference_lifetime_bounds;
            type SplitLast<#a_lt, #s: #state_trait_x> #reference_lifetime_bounds;
            type SplitLastMut<#a_lt, #s: #state_trait_x> #reference_lifetime_bounds;
            type Pop<#s: #state_trait_x> where #alignment_constraint;
            const NEEDS_DISCRIMINANT_FROM: bool;
            const NEEDS_DISCRIMINANT_TO: bool;
            // What is the minimum alignment guaranteed when this state is
            // reached?
            const MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD: usize;
            // What is the maximum alignment required for a future payload?
            const MAX_PAYLOAD_ALIGN: usize;
            const ID: usize;
            const NAME: &'static str;
            // not const-able: const fns in traits not yet supported
            fn popped<#s: #state_trait_x>(v: #vec_type #owned_gen_x_self2) -> Result<<Self as #state_trait_dx>::Pop<#s>, #vec_type #owned_gen_x_self2> where Self: #state_trait_x, #alignment_constraint;
            unsafe fn split_first<#a_lt, #e: #state_trait_x>(discriminants: &#a_lt [u8], payloads: &#a_lt [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitFirst<#a_lt, #e>>;
            unsafe fn split_first_mut<#a_lt, #e: #state_trait_x>(discriminants: &#a_lt mut [u8], payloads: &#a_lt mut [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitFirstMut<#a_lt, #e>>;
            unsafe fn split_last<#a_lt, #s: #state_trait_x>(discriminants: &#a_lt [u8], payloads: &#a_lt [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitLast<#a_lt, #s>>;
            unsafe fn split_last_mut<#a_lt, #s: #state_trait_x>(discriminants: &#a_lt mut [u8], payloads: &#a_lt mut [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitLastMut<#a_lt, #s>>;
            unsafe fn bump_slices_raw<#e: #state_trait_x, #p: #payload_trait #owned_gen_x_self1>(discriminants: &[u8], payloads: &[::std::mem::MaybeUninit<u8>]) -> (*const #p, usize, usize) where Self: #state_trait_x {
                let payload_size = ::std::mem::size_of::<#p>();
                let (before, _after) = payloads.split_at(payload_size);
                let stored_padding = <#e as #state_trait_dx>::MAX_PAYLOAD_ALIGN > <Self as #state_trait_dx>::MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD;
                let (d, p) = if stored_padding {
                    let padding = unsafe {
                        discriminants.get_unchecked(0)
                    };
                    let padding = (*padding as usize) * <Self as #state_trait_dx>::MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD;
                    (1, payload_size + padding)
                } else {
                    (0, payload_size)
                };
                (before.as_ptr().cast::<#p>(), d, p)
            }
            unsafe fn bump_slices<#a_lt, #b_lt, #e: #state_trait_x, #p: #payload_trait #owned_gen_x_self1>(discriminants: &#a_lt [u8], payloads: &#b_lt [::std::mem::MaybeUninit<u8>]) -> (*const #p, &#a_lt [u8], &#b_lt [::std::mem::MaybeUninit<u8>]) where Self: #state_trait_x {
                let (b, d, p) = <Self as #state_trait_dx>::bump_slices_raw::<#e, #p>(discriminants, payloads);
                (b, &discriminants[d..], &payloads[p..])
            }
            unsafe fn bump_slices_mut<#a_lt, #b_lt, #e: #state_trait_x, #p: #payload_trait #owned_gen_x_self1>(discriminants: &#a_lt mut [u8], payloads: &#b_lt mut [::std::mem::MaybeUninit<u8>]) -> (*mut #p, &#a_lt mut [u8], &#b_lt mut [::std::mem::MaybeUninit<u8>]) where Self: #state_trait_x {
                let (b, d, p) = <Self as #state_trait_dx>::bump_slices_raw::<#e, #p>(discriminants, payloads);
                (b as *mut _, &mut discriminants[d..], &mut payloads[p..])
            }
            unsafe fn v0tbump_slices_raw<#s: #state_trait_x, #p: #payload_trait #owned_gen_x_self2>(discriminants: &[u8], payloads: &[::std::mem::MaybeUninit<u8>]) -> (*const #p, usize, usize) where Self: #state_trait_x {
                let payload_size = ::std::mem::size_of::<#p>();
                let payload_offset = payloads.len() - payload_size;
                assert!(payload_offset < (isize::MAX as usize));
                let (before, after) = payloads.split_at(payload_offset);
                let stored_padding = <Self as #state_trait_dx>::MAX_PAYLOAD_ALIGN > <#s as #state_trait_dx>::MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD;
                let (d, p) = if stored_padding {
                    let padding = unsafe {
                        discriminants.last().unwrap_unchecked()
                    };
                    let padding = (*padding as usize) * <#s as #state_trait_dx>::MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD;
                    (discriminants.len() - 1, payload_offset - padding)
                } else {
                    (discriminants.len(), payload_offset)
                };
                (after.as_ptr().cast::<#p>(), d, p)
            }
            unsafe fn v0tbump_slices<#a_lt, #b_lt, #s: #state_trait_x, #p: #payload_trait #owned_gen_x_self2>(discriminants: &#a_lt [u8], payloads: &#b_lt [::std::mem::MaybeUninit<u8>]) -> (*const #p, &#a_lt [u8], &#b_lt [::std::mem::MaybeUninit<u8>]) where Self: #state_trait_x {
                let (b, d, p) = <Self as #state_trait_dx>::v0tbump_slices_raw::<#s, #p>(discriminants, payloads);
                (b, &discriminants[..d], &payloads[..p])
            }
            unsafe fn v0tbump_slices_mut<#a_lt, #b_lt, #s: #state_trait_x, #p: #payload_trait #owned_gen_x_self2>(discriminants: &#a_lt mut [u8], payloads: &#b_lt mut [::std::mem::MaybeUninit<u8>]) -> (*mut #p, &#a_lt mut [u8], &#b_lt mut [::std::mem::MaybeUninit<u8>]) where Self: #state_trait_x {
                let (b, d, p) = <Self as #state_trait_dx>::v0tbump_slices_raw::<#s, #p>(discriminants, payloads);
                (b as *mut _, &mut discriminants[..d], &mut payloads[..p])
            }
        }
    }
}

/// Expand to an implementation of the state trait for a type.
///
/// # Overview
///
/// This function takes the following arguments:
///
/// * `i`: the numerical ID of the state to implement the trait
/// * `state`: the state object itself
/// * `ctx`: the [FsmContext] to expand for
///
/// The expanded code defines a struct called `<state>` with the same generic
/// parameters passed in from the body of the [crate::samsara!] macro. It
/// then implements the trait `<id>State` for this struct.
pub fn implement_state_trait(i: usize, state: &FsmState, ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let vis = &def.visibility;
    let a_lt = &ctx.type_params.a_lt;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;
    let vec_type = &ctx.vec_type;
    let slice_type = &ctx.slice_type;
    let slice_type_mut = &ctx.slice_type_mut;
    let reference_lifetime_bounds = &ctx.reference_lifetime_bounds;
    let alignment_constraint = &ctx.modified_generics.alignment_constraint;

    let generics = &def.generics;
    let (geny, genx, genw) = generics.split_for_impl();
    let borrowed_gen = &ctx.modified_generics.borrowed_gen;
    let (_, borrowed_gen_x, _) = borrowed_gen.split_for_impl();
    let owned_gen = &ctx.modified_generics.owned_gen;
    let (_, owned_gen_x, _) = owned_gen.split_for_impl();
    let end_borrowed_gen = &ctx.modified_generics.end_borrowed_gen;
    let start_borrowed_gen = &ctx.modified_generics.start_borrowed_gen;
    let start_owned_gen = &ctx.modified_generics.start_owned_gen;
    let (_, from_tpx, _) = end_borrowed_gen.split_for_impl();
    let (_, to_tpx, _) = start_borrowed_gen.split_for_impl();
    let (_, to_tp_px, _) = start_owned_gen.split_for_impl();

    let selfid = format_ident!("Self");
    let payload_trait = &ctx.payload_trait;

    let state_trait_x = &ctx.modified_generics.state_trait_x;
    let state_trait_dx = &ctx.state_trait_dx;
    let owned_gen_x_self2 = Subst(&owned_gen_x, e, &selfid);

    let state_attrs = &state.attrs;
    let state_name = &state.name;
    let name_str = state_name.to_string();
    let (from_states, from_payloads): (Vec<_>, Vec<_>) = ctx.transitions_beginning_at[i]
        .iter()
        .map(|tr| {
            let FsmTransition { payload, end, .. } = &def.transitions[*tr];
            let state_id = &def.states[*end];
            (state_id, payload)
        })
        .unzip();
    let (to_states, to_payloads): (Vec<_>, Vec<_>) = ctx.transitions_ending_at[i]
        .iter()
        .map(|tr| {
            let FsmTransition { start, payload, .. } = &def.transitions[*tr];
            let state_id = &def.states[*start];
            (state_id, payload)
        })
        .unzip();
    let from_state_names: Vec<_> = from_states.iter().map(|s| &s.name).collect();
    let to_state_names: Vec<_> = to_states.iter().map(|s| &s.name).collect();

    let rev_suffix = def.direction.suffix();
    let split_first = format_ident!("{}SplitFirst{}", state_name, rev_suffix);
    let split_first_mut = format_ident!("{}SplitFirstMut{}", state_name, rev_suffix);
    let split_last = format_ident!("{}SplitLast{}", state_name, rev_suffix);
    let split_last_mut = format_ident!("{}SplitLastMut{}", state_name, rev_suffix);
    let pop = format_ident!("{}Pop{}", state_name, rev_suffix);

    let from_tpu_m: Vec<_> = from_state_names
        .iter()
        .copied()
        .map(|state_name| {
            let full_state_type = quote! {#state_name #genx};
            SubstN(&borrowed_gen_x, s, full_state_type)
        })
        .collect();
    let to_tpu_m: Vec<_> = to_state_names
        .iter()
        .copied()
        .map(|state_name| {
            let full_state_type = quote! {#state_name #genx};
            SubstN(&borrowed_gen_x, e, full_state_type)
        })
        .collect();
    let to_tp_pu_m = to_state_names.iter().copied().map(|state_name| {
        let full_state_type = quote! {#state_name #genx};
        SubstN(&owned_gen_x, e, full_state_type)
    });
    let from_tp = (!from_states.is_empty()).then(|| &from_tpx);
    let to_tp = (!to_states.is_empty()).then(|| &to_tpx);
    let to_tp_p = (!to_states.is_empty()).then(|| &to_tp_px);
    let from_tpf = (!from_states.is_empty()).then(|| &end_borrowed_gen);
    let to_tpf = (!to_states.is_empty()).then(|| &start_borrowed_gen);
    let to_tp_pf = (!to_states.is_empty()).then(|| &start_owned_gen);
    let from_tpwc = from_tpf.and_then(|d| d.where_clause.as_ref());
    let to_tpwc = to_tpf.and_then(|d| d.where_clause.as_ref());
    let to_tp_pwc = to_tp_pf.and_then(|d| d.where_clause.as_ref());
    let to_tp_pwc =
        add_alignment_constraint(to_tp_pwc, &ctx.modified_generics.alignment_constraint);

    let from_tpu_md = from_state_names.iter().map(|state_name| {
        let full_state_type = quote! {#state_name #genx};
        Subst2(&owned_gen_x, s, &selfid, e, full_state_type)
    });
    let to_tpu_md = to_state_names.iter().map(|state_name| {
        let full_state_type = quote! {#state_name #genx};
        Subst2(&owned_gen_x, e, &selfid, s, full_state_type)
    });
    let needs_discriminant_from = quote! {
        #(<#from_payloads as #payload_trait #from_tpu_md>
            ::NEEDS_DISCRIMINANT ||)*
        false
    };
    let needs_discriminant_to = quote! {
        #(<#to_payloads as #payload_trait #to_tpu_md>
            ::NEEDS_DISCRIMINANT ||)*
        false
    };

    // Code generated for destructuring methods
    // pop
    let (pop_clauses, ind_action_pop) = ctx.end_items(i, |actx| pop_action(actx, vec_type, e));
    // split_first
    let (split_first_clauses, ind_action_split_first) =
        ctx.start_items(i, |actx| split_first_action(actx, slice_type, false));
    // split_first_mut
    let (split_first_mut_clauses, ind_action_split_first_mut) =
        ctx.start_items(i, |actx| split_first_action(actx, slice_type_mut, true));
    // split_last
    let (split_last_clauses, ind_action_split_last) =
        ctx.end_items(i, |actx| split_last_action(actx, slice_type, false));
    // split_last_mut
    let (split_last_mut_clauses, ind_action_split_last_mut) =
        ctx.end_items(i, |actx| split_last_action(actx, slice_type_mut, true));

    let vec_type_phantom_decls = def.generate_phantom_decls_for_owned_type();
    let tag_type_def = (def.direction == Direction::Fwd).then(|| {
        quote! {
            #(#state_attrs)*
            #[derive(Copy, Clone)]
            #vis struct #state_name #generics {
                #(#vec_type_phantom_decls,)*
            }
        }
    });

    quote! {
        #tag_type_def

        #[derive(Copy, Clone)]
        #vis enum #split_first #from_tpf #from_tpwc {
            #(#from_state_names(&#a_lt #from_payloads, #slice_type #from_tpu_m),)*
        }
        #vis enum #split_first_mut #from_tpf #from_tpwc {
            #(#from_state_names(&#a_lt mut #from_payloads, #slice_type_mut #from_tpu_m),)*
        }
        #[derive(Copy, Clone)]
        #vis enum #split_last #to_tpf #to_tpwc {
            #(#to_state_names(#slice_type #to_tpu_m, &#a_lt #to_payloads),)*
        }
        #vis enum #split_last_mut #to_tpf #to_tpwc {
            #(#to_state_names(#slice_type_mut #to_tpu_m, &#a_lt mut #to_payloads),)*
        }
        #vis enum #pop #to_tp_pf #to_tp_pwc {
            #(#to_state_names(#vec_type #to_tp_pu_m, #to_payloads),)*
        }
        unsafe impl #geny #state_trait_dx for #state_name #genx #genw {
            type SplitFirst<#a_lt, #e: #state_trait_x> = #split_first #from_tp #reference_lifetime_bounds;
            type SplitFirstMut<#a_lt, #e: #state_trait_x> = #split_first_mut #from_tp #reference_lifetime_bounds;
            type SplitLast<#a_lt, #s: #state_trait_x> = #split_last #to_tp #reference_lifetime_bounds;
            type SplitLastMut<#a_lt, #s: #state_trait_x> = #split_last_mut #to_tp #reference_lifetime_bounds;
            type Pop<#s: #state_trait_x> = #pop #to_tp_p where #alignment_constraint;
            const NEEDS_DISCRIMINANT_FROM: bool = #needs_discriminant_from;
            const NEEDS_DISCRIMINANT_TO: bool = #needs_discriminant_to;
            const MIN_ALIGN_AFTER_PREVIOUS_PAYLOAD: usize = ::samsara::myn(&[
                #(::std::mem::align_of::<#to_payloads>()),*
            ]);
            const MAX_PAYLOAD_ALIGN: usize = ::samsara::macks(&[
                #(::std::mem::align_of::<#from_payloads>()),*
            ]);
            const ID: usize = #i;
            const NAME: &'static str = #name_str;
            // The Self: #state_trait_x bound causes this code to fail
            // to compile under certain tests (particularly generic
            // and uncanny). Leaving it out doesn’t impact tests that
            // use #[samsara(bidirectional)].
            #[allow(unreachable_code)]
            fn popped<#s: #state_trait_x>(v: #vec_type #owned_gen_x_self2) -> Result<<Self as #state_trait_dx>::Pop<#s>, #vec_type #owned_gen_x_self2> where /*Self: #state_trait_x, */#alignment_constraint {
                if v.is_empty() {
                    return Err(v);
                }
                let (mut discriminants, mut payloads) = v.into_parts();
                // Did we put a discriminant byte?
                let should_pop_discriminant = <Self as #state_trait_dx>::NEEDS_DISCRIMINANT_TO;
                Ok(if should_pop_discriminant {
                    let discriminant = unsafe {
                        discriminants.pop().unwrap_unchecked()
                    };
                    match discriminant {
                        #pop_clauses
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                } else {
                    // We don’t care about the discriminant
                    #ind_action_pop
                })
            }
            #[allow(unreachable_code)]
            unsafe fn split_first<#a_lt, #e: #state_trait_x>(discriminants: &#a_lt [u8], payloads: &#a_lt [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitFirst<#a_lt, #e>> {
                if discriminants.is_empty() && payloads.is_empty() {
                    return None;
                }
                // Did we put a discriminant byte?
                let should_pop_discriminant = <Self as #state_trait_dx>::NEEDS_DISCRIMINANT_FROM;
                Some(if should_pop_discriminant {
                    let (discriminant, discriminants) = unsafe {
                        discriminants.split_first().unwrap_unchecked()
                    };
                    match discriminant {
                        #split_first_clauses
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                } else {
                    #ind_action_split_first
                })
            }
            #[allow(unreachable_code)]
            unsafe fn split_first_mut<#a_lt, #e: #state_trait_x>(discriminants: &#a_lt mut [u8], payloads: &#a_lt mut [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitFirstMut<#a_lt, #e>> {
                if discriminants.is_empty() && payloads.is_empty() {
                    return None;
                }
                // Did we put a discriminant byte?
                let should_pop_discriminant = <Self as #state_trait_dx>::NEEDS_DISCRIMINANT_FROM;
                Some(if should_pop_discriminant {
                    let (discriminant, discriminants) = unsafe {
                        discriminants.split_first_mut().unwrap_unchecked()
                    };
                    match discriminant {
                        #split_first_mut_clauses
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                } else {
                    #ind_action_split_first_mut
                })
            }
            #[allow(unreachable_code)]
            unsafe fn split_last<#a_lt, #s: #state_trait_x>(discriminants: &#a_lt [u8], payloads: &#a_lt [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitLast<#a_lt, #s>> {
                if discriminants.is_empty() && payloads.is_empty() {
                    return None;
                }
                let should_pop_discriminant = <Self as #state_trait_dx>::NEEDS_DISCRIMINANT_TO;
                Some(if should_pop_discriminant {
                    let (discriminant, discriminants) = unsafe {
                        discriminants.split_last().unwrap_unchecked()
                    };
                    match discriminant {
                        #split_last_clauses
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                } else {
                    #ind_action_split_last
                })
            }
            #[allow(unreachable_code)]
            unsafe fn split_last_mut<#a_lt, #s: #state_trait_x>(discriminants: &#a_lt mut [u8], payloads: &#a_lt mut [::std::mem::MaybeUninit<u8>]) -> Option<Self::SplitLastMut<#a_lt, #s>> {
                if discriminants.is_empty() && payloads.is_empty() {
                    return None;
                }
                let should_pop_discriminant = <Self as #state_trait_dx>::NEEDS_DISCRIMINANT_TO;
                Some(if should_pop_discriminant {
                    let (discriminant, discriminants) = unsafe {
                        discriminants.split_last_mut().unwrap_unchecked()
                    };
                    match discriminant {
                        #split_last_mut_clauses
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                } else {
                    #ind_action_split_last_mut
                })
            }
        }
    }
}

fn pop_action(ctx: ActionContextEnd, vec_type: &Ident, e: &Ident) -> TokenStream {
    let ActionContextEnd {
        payload,
        cur_state,
        prev_state,
        typarams,
        full_typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    let full_typarams = full_typarams.as_turbofish();
    let full_tp_subst = SubstN(&full_typarams, e, quote! {#prev_state #tf});
    quote!({
        let (payload_ptr, discriminants_end, payloads_end) = unsafe {
            <#cur_state #typarams as #state_trait_x>::v0tbump_slices_raw::<#prev_state #tf, #payload>(&discriminants, &payloads)
        };
        discriminants.truncate(discriminants_end);
        payloads.truncate(payloads_end);
        unsafe {
            Self::Pop::#prev_state(
                #vec_type #full_tp_subst::from_parts(discriminants, payloads),
                ::std::ptr::read(payload_ptr)
            )
        }
    })
}

fn split_first_action(ctx: ActionContextStart, slice_type: &Ident, mutates: bool) -> TokenStream {
    let ActionContextStart {
        payload,
        cur_state,
        next_state,
        typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    let m = |s: &str| {
        if mutates {
            format_ident!("{}_mut", s)
        } else {
            format_ident!("{}", s)
        }
    };
    let bump_slices = m("bump_slices");
    let return_type_name = if mutates {
        quote! { SplitFirstMut }
    } else {
        quote! { SplitFirst }
    };
    let mut_token = if mutates {
        quote! { mut }
    } else {
        quote! {}
    };
    quote!({
        let (payload_ptr, discriminants, payloads) = unsafe {
            <#cur_state #typarams as #state_trait_x>::#bump_slices::<#next_state #tf, #payload>(discriminants, payloads)
        };
        let payload_ref = unsafe { &#mut_token *payload_ptr };
        Self::#return_type_name::#next_state(
            payload_ref,
            unsafe {
                #slice_type::from_raw_parts(
                    discriminants,
                    payloads)
            },
        )
    })
}

fn split_last_action(ctx: ActionContextEnd, slice_type: &Ident, mutates: bool) -> TokenStream {
    let ActionContextEnd {
        payload,
        cur_state,
        prev_state,
        typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    let m = |s: &str| {
        if mutates {
            format_ident!("{}_mut", s)
        } else {
            format_ident!("{}", s)
        }
    };
    let v0tbump_slices = m("v0tbump_slices");
    let return_type_name = if mutates {
        quote! { SplitLastMut }
    } else {
        quote! { SplitLast }
    };
    let mut_token = if mutates {
        quote! { mut }
    } else {
        quote! {}
    };
    quote!({
        let (payload_ptr, discriminants, payloads) = unsafe {
            <#cur_state #typarams as #state_trait_x>::#v0tbump_slices::<#prev_state #tf, #payload>(discriminants, payloads)
        };
        let payload_ref = unsafe {
            &#mut_token *payload_ptr
        };
        Self::#return_type_name::#prev_state(
            unsafe {
                #slice_type::from_raw_parts(
                    discriminants,
                    payloads)
            },
            payload_ref,
        )
    })
}
