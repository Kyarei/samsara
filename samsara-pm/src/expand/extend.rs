//! Implements methods for extending collections.

use proc_macro2::TokenStream;
use quote::{format_ident, quote};
use syn::Ident;

use crate::{actx::ActionContextStart, ctx::FsmContext, generics::SubstN};

/// Expand to an implementation of `PathBuf::extend_raw` or `PathBuf::extend_raw_from_slice`.
///
/// `extend_raw` is generated when the parameter `clone` is false and moves the
/// the elements to be appended.
///
/// `extend_raw_from_slice` is generated when `clone` is true. It clones all
/// of the elements to be appended, so it requires all payloads to implement
/// [::std::clone::Clone].
///
/// Both of these methods are low-level and do not depend on `S` or `E`.
pub fn expand_extend_raw(clone: bool, ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let id = &def.ident;
    let (_, genx, _) = def.generics.split_for_impl();
    let genxtf = genx.as_turbofish();
    let vec_type = &ctx.vec_type;
    let state_trait_dx = &ctx.state_trait_dx;
    let payload_container = &ctx.modified_generics.payload_container;
    let e = &ctx.type_params.e;

    let optcond = if clone {
        let payloads = def.transitions.iter().map(|p| &p.payload);
        quote! { #id #genxtf::UNIFORMLY_ALIGNED #(&& ::impls::impls!(#payloads: Copy))* }
    } else {
        quote! { #id #genxtf::UNIFORMLY_ALIGNED }
    };
    let method_name = if clone {
        format_ident!("extend_from_slice_raw")
    } else {
        format_ident!("extend_raw")
    };
    let outer_clauses = def.states.iter().enumerate().map(|(i, state)| {
        let state_name = &state.name;
        let (clauses, ind_action) = ctx.start_items(
            i,
            |ctx| extend_raw_action(ctx, &vec_type, e, clone),
        );
        quote! {
            #i => {
                let should_pop_discriminant = <#state_name #genx as #state_trait_dx>::NEEDS_DISCRIMINANT_FROM;
                if should_pop_discriminant {
                    let discriminant;
                    (discriminant, discriminants_src) = unsafe {
                        discriminants_src.split_first().unwrap_unchecked()
                    };
                    match discriminant {
                        #clauses
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                } else {
                    #ind_action
                }
            }
        }
    });
    quote! {
        unsafe fn #method_name(
            discriminants_dst: &mut ::std::vec::Vec<u8>,
            payloads_dst: &mut #payload_container,
            mut discriminants_src: &[u8],
            mut payloads_src: &[::std::mem::MaybeUninit<u8>],
            state_start: usize,
            state_end: usize,
        ) {
            #[allow(unreachable_code)]
            if #optcond {
                discriminants_dst.extend_from_slice(discriminants_src);
                payloads_dst.extend_from_slice(payloads_src);
            } else {
                let mut id = state_start;
                while !discriminants_src.is_empty() || !payloads_src.is_empty() {
                    match id {
                        #(#outer_clauses,)*
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                }
                debug_assert!(id == state_end);
            }
        }
    }
}

fn extend_raw_action(
    ctx: ActionContextStart,
    vec_type: &Ident,
    e: &Ident,
    clone: bool,
) -> TokenStream {
    let ActionContextStart {
        payload,
        cur_state,
        next_state,
        next_state_id: end_id,
        typarams,
        full_typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    let payload_expr = if clone {
        quote! {
            Clone::clone(&*payload_ptr)
        }
    } else {
        quote! {
            ::std::ptr::read(payload_ptr)
        }
    };
    let full_typarams = full_typarams.as_turbofish();
    let next_vec_type_params = SubstN(&full_typarams, e, quote! { #cur_state #typarams });
    quote!({
        let payload_ptr;
        (payload_ptr, discriminants_src, payloads_src) = unsafe {
            <#cur_state #typarams as #state_trait_x>::bump_slices::<#next_state #tf, #payload>(discriminants_src, payloads_src)
        };
        let payload = unsafe { #payload_expr };
        #vec_type #next_vec_type_params::push_raw::<#next_state #typarams, #payload>(discriminants_dst, payloads_dst, payload);
        id = #end_id;
    })
}
