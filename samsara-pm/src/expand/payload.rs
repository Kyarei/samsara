//! Functions for expanding the payload trait definition and implementations.

use proc_macro2::{TokenStream, TokenTree};
use quote::{quote, ToTokens, TokenStreamExt};

use crate::{ctx::FsmContext, def::FsmTransition};

/// Expand to the definition for the trait for payloads of transitions in the
/// finite state machine.
///
/// The trait is named `<id>Payload` (abbreviated as `Payload` hereafter). In
/// addition to the generic parameters passed in by the user, this trait
/// contains two additional type parameters for the start and end states.
/// It contains two associated constants:
///
/// * `NEEDS_DISCRIMINANT`: true if this transition needs a discriminant to
///   distinguish it from other transitions from the start state or to the
///   end state, or from the absence of such a transition
/// * `DISCRIMINANT`: the discriminant used if a discriminant is needed. This
///   is set to the index of the transition in `def.transitions`.
pub fn declare_payload_trait(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let vis = &def.visibility;
    let payload_trait = &ctx.payload_trait;
    let (owned_gen_y, _, owned_gen_w) = ctx.modified_generics.owned_gen.split_for_impl();
    quote! {
        #vis trait #payload_trait #owned_gen_y #owned_gen_w {
            const NEEDS_DISCRIMINANT: bool;
            const DISCRIMINANT: u8;
        }
    }
}

/// Expand to an implementation of the payload trait for a type.
///
/// # Overview
///
/// This function takes the following arguments:
///
/// * `i`: the numerical ID of the transition whose payload should implement
///   the trait
/// * `transition`: the transition in question
/// * `ctx`: the [FsmContext] to expand for
pub fn implement_payload_trait(
    i: usize,
    transition: &FsmTransition,
    ctx: &FsmContext,
) -> TokenStream {
    let def = &ctx.def;
    let (geny, genx, genw) = def.generics.split_for_impl();
    let (_, owned_gen_x, _) = ctx.modified_generics.owned_gen.split_for_impl();
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;
    let payload_trait = &ctx.payload_trait;

    let from = &def.states[transition.start].name;
    let to = &def.states[transition.end].name;
    let mut owned_gen_x2 = TokenStream::new();
    for tt in owned_gen_x.to_token_stream().into_iter() {
        match tt {
            TokenTree::Ident(id) if id == *s => {
                owned_gen_x2.append(from.clone());
                genx.to_tokens(&mut owned_gen_x2);
            }
            TokenTree::Ident(id) if id == *e => {
                owned_gen_x2.append(to.clone());
                genx.to_tokens(&mut owned_gen_x2);
            }
            s => {
                owned_gen_x2.append(s);
            }
        }
    }
    let needs_discriminant = ctx.transitions_beginning_at[transition.start].len() > 1
        || ctx.transitions_ending_at[transition.end].len() > 1;
    let trans_idx = u8::try_from(i).expect("Disciminant did not fit in a u8");

    let attrs = &transition.attrs;
    let payload = &transition.payload;

    quote! {
        #(#attrs)*
        impl #geny #payload_trait #owned_gen_x2 for #payload #genw {
            const NEEDS_DISCRIMINANT: bool =
                #needs_discriminant ||
                    ::std::mem::size_of::<#payload>() == 0;
            const DISCRIMINANT: u8 = #trans_idx;
        }
    }
}
