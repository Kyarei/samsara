//! Expand the front matter.

use proc_macro2::TokenStream;
use quote::quote;

use crate::FsmContext;

/// Expand to the front matter for the FSM.
///
/// This consists of the `<id>` structure, whose sole purpose is to hold
/// associated constants related to the layout of payloads. In particular, it
/// defines two associated constants:
///
/// * `MAX_ALIGN`: the maximum alignment of any of the payloads
/// * `UNIFORMLY_ALIGNED`: true if and only if all of the payloads have the
///   same alignment – used for some optimizations.
///
/// All attributes declared on the FSM definition itself land on this struct
/// definition as well.
pub fn expand_front_matter(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let main_attrs = &def.attrs;
    let id = &def.ident;
    let generics = &def.generics;
    let vis = &def.visibility;
    let vec_type_phantom_decls = def.generate_phantom_decls_for_owned_type();
    let (geny, genx, genw) = generics.split_for_impl();

    let alignments = def
        .transitions
        .iter()
        .map(|trans| {
            let payload = &trans.payload;
            quote! { ::std::mem::align_of::<#payload>() }
        })
        .collect::<Vec<_>>();

    quote! {
        // Used as a namespace for items that don’t depend on start and end
        // states
        #(#main_attrs,)*
        #vis struct #id #generics {
            #(#vec_type_phantom_decls,)*
        }
        impl #geny #id #genx #genw {
            // Constants:
            // Maximum alignment of all payloads
            pub const MAX_ALIGN: usize = ::samsara::macks(&[#(#alignments),*]);
            // Do all payloads have the same alignment?
            pub const UNIFORMLY_ALIGNED: bool = ::samsara::are_same(&[#(#alignments),*]);
        }
    }
}
