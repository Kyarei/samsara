//! Functions for adding additional constraints to `where`-clauses.

use proc_macro2::TokenStream;
use quote::quote;
use syn::{Generics, Path, WhereClause};

/// Add an alignment constraint.
pub fn add_alignment_constraint(
    wc: Option<&WhereClause>,
    alignment_constraint: &TokenStream,
) -> TokenStream {
    match wc {
        None => quote! {
            where #alignment_constraint
        },
        Some(s) => {
            let no_comma = s.predicates.empty_or_trailing();
            let c = (!no_comma).then(|| quote!(,));
            quote! {
                #s #c #alignment_constraint
            }
        }
    }
}

/// Add constraints that require every type parameter of a generics object
/// to implement a trait.
pub fn add_trait_constraints(
    generics: &Generics,
    where_clause: &TokenStream,
    trait_name: &Path,
) -> TokenStream {
    let tids = generics.type_params().map(|g| &g.ident);
    quote! {
        #where_clause,
            #(#tids: #trait_name),*
    }
}
