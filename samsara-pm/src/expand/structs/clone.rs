//! Implement [::std::clone::Clone] for `<id>PathBuf`.

use proc_macro2::TokenStream;
use quote::quote;
use syn_path::path;

use crate::ctx::FsmContext;

use crate::def::DeriveFlags;
use crate::expand::addc::{add_alignment_constraint, add_trait_constraints};
use crate::expand::extend::expand_extend_raw;
use crate::generics::Subst;

/// Expands to an implementation of [::std::clone::Clone] for `<id>PathBuf`.
///
/// If the definition was not set to derive this trait, then
/// this function returns `None`.
pub fn impl_clone(ctx: &FsmContext) -> Option<TokenStream> {
    let def = ctx.def;
    let vec_type = &ctx.vec_type;
    let state_trait_x = &ctx.modified_generics.state_trait_x;
    let state_trait_dx = &ctx.state_trait_dx;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;
    let t = &ctx.type_params.t;
    let slice_type = &ctx.slice_type;
    let slice_type_mut = &ctx.slice_type_mut;

    let (owned_gen_y, owned_gen_x, owned_gen_w) = ctx.modified_generics.owned_gen.split_for_impl();
    let alignment_constraint = &ctx.modified_generics.alignment_constraint;
    let owned_gen_w_with_align_constraint =
        add_alignment_constraint(owned_gen_w, alignment_constraint);
    let owned_gen_x_t2 = Subst(&owned_gen_x, e, t);
    let owned_gen_x_s2 = Subst(&owned_gen_x, e, s);

    let owned_gen_w_with_align_and_clone_constraints = add_trait_constraints(
        &def.generics,
        &owned_gen_w_with_align_constraint,
        &path!(::std::clone::Clone),
    );

    def.settings.derive_flags.contains(DeriveFlags::CLONE).then(|| {
        let extend_from_slice_raw = expand_extend_raw(true, &ctx);

        quote! {
            impl #owned_gen_y #vec_type #owned_gen_x #owned_gen_w_with_align_and_clone_constraints {
                #extend_from_slice_raw

                pub fn extended_from_slice<#t: #state_trait_x>(self, slice: &#slice_type #owned_gen_x_t2) -> #vec_type #owned_gen_x_t2 {
                    let (mut discriminants_dst, mut payloads_dst) = self.into_parts();

                    unsafe {
                        Self::extend_from_slice_raw(
                            &mut discriminants_dst, &mut payloads_dst,
                            slice.discriminants, slice.payloads,
                            <#e as #state_trait_dx>::ID, <#t as #state_trait_dx>::ID);
                        #vec_type::from_parts(discriminants_dst, payloads_dst)
                    }
                }

                pub fn extended_from_slice_mut<#t: #state_trait_x>(self, slice: &#slice_type_mut #owned_gen_x_t2) -> #vec_type #owned_gen_x_t2 {
                    self.extended_from_slice(&#slice_type::from(slice))
                }

                pub fn from_slice(slice: &#slice_type #owned_gen_x) -> Self {
                    #vec_type::#owned_gen_x_s2::new().extended_from_slice(slice)
                }
                pub fn from_slice_mut(slice: &#slice_type_mut #owned_gen_x) -> Self {
                    #vec_type::#owned_gen_x_s2::new().extended_from_slice_mut(slice)
                }
            }
            impl #owned_gen_y Clone for #vec_type #owned_gen_x #owned_gen_w_with_align_and_clone_constraints {
                fn clone(&self) -> Self {
                    Self::from_slice(&self.view())
                }
            }
        }
    })
}
