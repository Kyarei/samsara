//! Implement [::std::ops::Drop] for `<id>PathBuf`.

use proc_macro2::TokenStream;
use quote::quote;

use crate::actx::ActionContextEnd;
use crate::ctx::FsmContext;

use crate::expand::addc::add_alignment_constraint;

/// Expands to an implementation of [::std::ops::Drop] for `<id>PathBuf`.
///
/// This implementation iterates forward through the path and drops each
/// payload in that order.
pub fn impl_drop(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let vec_type = &ctx.vec_type;
    let state_trait_dx = &ctx.state_trait_dx;
    let s = &ctx.type_params.s;
    let e = &ctx.type_params.e;

    let (_, genx, _) = def.generics.split_for_impl();
    let (owned_gen_y, owned_gen_x, owned_gen_w) = ctx.modified_generics.owned_gen.split_for_impl();
    let alignment_constraint = &ctx.modified_generics.alignment_constraint;
    let owned_gen_w_with_align_constraint =
        add_alignment_constraint(owned_gen_w, alignment_constraint);

    let outer_clauses = def.states.iter().enumerate().map(|(i, state)| {
        let state_name = &state.name;
        // Used when a discriminant is used
        let (drop_clauses, ind_action) = ctx.end_items(
            i,
            drop_action,
        );
        quote! {
            #i => {
                let should_pop_discriminant = <#state_name #genx as #state_trait_dx>::NEEDS_DISCRIMINANT_TO;
                if should_pop_discriminant {
                    // eprintln!("Using discriminant");
                    let discriminant;
                    (discriminant, discriminants) = unsafe {
                        discriminants.split_last_mut().unwrap_unchecked()
                    };
                    match discriminant {
                        #drop_clauses
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                } else {
                    // eprintln!("Not using discriminant");
                    #ind_action
                }
            }
        }
    });
    let optcond = {
        let payloads = def.transitions.iter().map(|p| &p.payload);
        quote! { true #(&& ::impls::impls!(#payloads: Copy))* }
    };
    quote! {
        impl #owned_gen_y ::std::ops::Drop for #vec_type #owned_gen_x #owned_gen_w_with_align_constraint {
            // Brace yourselves.
            // We can’t repeatedly use popped because that changes the
            // type of the collection.
            #[allow(unreachable_code)]
            fn drop(&mut self) {
                if #optcond { return; }
                let mut id = <#e as #state_trait_dx>::ID;
                let mut payloads = &mut self.payloads[..];
                let mut discriminants = &mut self.discriminants[..];
                while !payloads.is_empty() || !discriminants.is_empty() {
                    // dbg!(payloads as &_);
                    // dbg!(discriminants as &_);
                    // dbg!(id);
                    match id {
                        #(#outer_clauses,)*
                        _ => unsafe { ::std::hint::unreachable_unchecked() },
                    }
                }
                // dbg!(id);
                debug_assert!(id == <#s as #state_trait_dx>::ID);
            }
        }
    }
}

fn drop_action(ctx: ActionContextEnd) -> TokenStream {
    let ActionContextEnd {
        payload,
        cur_state,
        prev_state,
        prev_state_id: start_id,
        typarams,
        state_trait_x,
        ..
    } = ctx;
    let tf = typarams.as_turbofish();
    quote!({
        let payload_ptr;
        (payload_ptr, discriminants, payloads) = unsafe {
            <#cur_state #typarams as #state_trait_x>::v0tbump_slices_mut::<#prev_state #tf, #payload>(discriminants, payloads)
        };
        unsafe {
            ::std::ptr::drop_in_place(payload_ptr);
        }
        id = #start_id;
    })
}
