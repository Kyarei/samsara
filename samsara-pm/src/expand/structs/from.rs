//! Implement slicing conversions.

use proc_macro2::TokenStream;
use quote::quote;

use crate::ctx::FsmContext;

use crate::expand::addc::add_alignment_constraint;

/// Expand to implementations of slicing conversions.
///
/// This method produces three implementations of `From`:
///
/// * `Path<'a, S, E>: From<&'a PathBuf<S, E>>`
/// * `Path<'a, S, E>: From<&'a PathMut<'a, S, E>>`
/// * `PathMut<'a, S, E>: From<&'a PathBuf<S, E>>`
///
/// These implementations cannot be in terms of `Deref` or `Borrow` because
/// the view types are not references, and the second one cannot be implicit
/// for the same reason.
pub fn expand_slicing_conversions(ctx: &FsmContext) -> TokenStream {
    let def = &ctx.def;
    let slice_type = &ctx.slice_type;
    let slice_type_mut = &ctx.slice_type_mut;
    let vec_type = &ctx.vec_type;
    let a_lt = &ctx.type_params.a_lt;

    let (borrowed_gen_y, borrowed_gen_x, borrowed_gen_w) =
        ctx.modified_generics.borrowed_gen.split_for_impl();
    let (_, owned_gen_x, _) = ctx.modified_generics.owned_gen.split_for_impl();
    let alignment_constraint = &ctx.modified_generics.alignment_constraint;
    let borrowed_gen_w_with_align_constraint =
        add_alignment_constraint(borrowed_gen_w, alignment_constraint);

    let vec_type_phantom_uses: Vec<_> = def.generate_phantom_fields().collect();

    quote! {
        impl #borrowed_gen_y
        ::std::convert::From<&#a_lt #vec_type #owned_gen_x> for #slice_type #borrowed_gen_x #borrowed_gen_w_with_align_constraint {
            fn from(t: &#a_lt #vec_type #owned_gen_x) -> Self {
                #slice_type {
                    discriminants: &t.discriminants,
                    payloads: &t.payloads,
                    phantom: t.phantom,
                    #(#vec_type_phantom_uses,)*
                }
            }
        }
        impl #borrowed_gen_y
        ::std::convert::From<&#a_lt #slice_type_mut #borrowed_gen_x> for #slice_type #borrowed_gen_x #borrowed_gen_w {
            fn from(t: &#a_lt #slice_type_mut #borrowed_gen_x) -> Self {
                #slice_type {
                    discriminants: t.discriminants,
                    payloads: t.payloads,
                    phantom: t.phantom,
                    #(#vec_type_phantom_uses,)*
                }
            }
        }
        impl #borrowed_gen_y
        ::std::convert::From<&#a_lt mut #vec_type #owned_gen_x> for #slice_type_mut #borrowed_gen_x #borrowed_gen_w_with_align_constraint {
            fn from(t: &#a_lt mut #vec_type #owned_gen_x) -> Self {
                #slice_type_mut {
                    discriminants: &mut t.discriminants,
                    payloads: &mut t.payloads,
                    phantom: t.phantom,
                    #(#vec_type_phantom_uses,)*
                }
            }
        }
    }
}
