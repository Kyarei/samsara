//! Operations involving generics.

use std::collections::HashSet;

use proc_macro2::TokenTree;
use quote::{ToTokens, TokenStreamExt};
use syn::{
    punctuated::Punctuated,
    visit::{self, Visit},
    Generics, Ident, Lifetime, Path, WhereClause,
};

/// Combines two [WhereClause]s together.
///
/// This creates a new [WhereClause] with the predicates of both inputs.
fn combine_where(elt: WhereClause, saal: WhereClause) -> WhereClause {
    WhereClause {
        where_token: elt.where_token,
        predicates: Iterator::chain(elt.predicates.into_iter(), saal.predicates.into_iter())
            .collect(),
    }
}

/// Combines two [Generics] objects together.
///
/// The resulting object contains the type parameters and constraints of both
/// inputs.
pub fn combine(elt: Generics, saal: Generics) -> Generics {
    let mut lifetimes = Vec::new();
    let mut types = Vec::new();
    let mut consts = Vec::new();
    for param in Iterator::chain(elt.params.into_iter(), saal.params.into_iter()) {
        match param {
            t @ syn::GenericParam::Type(_) => types.push(t),
            l @ syn::GenericParam::Lifetime(_) => lifetimes.push(l),
            c @ syn::GenericParam::Const(_) => consts.push(c),
        }
    }
    let mut params = Punctuated::new();
    params.extend(lifetimes);
    params.extend(types);
    params.extend(consts);
    let where_clause = match (elt.where_clause, saal.where_clause) {
        (Some(c1), Some(c2)) => Some(combine_where(c1, c2)),
        (c, None) => c,
        (None, c) => c,
    };
    Generics {
        lt_token: elt.lt_token.or(saal.lt_token),
        params,
        gt_token: elt.gt_token.or(saal.gt_token),
        where_clause,
    }
}

struct FilterVisitor<'a> {
    unused_types: &'a mut HashSet<Ident>,
    unused_lifetimes: &'a mut HashSet<Ident>,
}

impl<'ast> Visit<'ast> for FilterVisitor<'ast> {
    fn visit_path(&mut self, path: &'ast Path) {
        if let Some(id) = path.get_ident() {
            self.unused_types.remove(id); // it’s used now
        }
        visit::visit_path(self, path);
    }
    fn visit_lifetime(&mut self, lt: &'ast Lifetime) {
        self.unused_lifetimes.remove(&lt.ident); // it’s used now
        visit::visit_lifetime(self, lt);
    }
}

struct FindVisitor<'a> {
    unused_types: &'a HashSet<Ident>,
    unused_lifetimes: &'a HashSet<Ident>,
    found: bool,
}

impl<'ast> Visit<'ast> for FindVisitor<'ast> {
    fn visit_path(&mut self, path: &'ast Path) {
        if let Some(id) = path.get_ident() {
            if self.unused_types.contains(id) {
                self.found = true;
            }
        }
        visit::visit_path(self, path);
    }
    fn visit_lifetime(&mut self, lt: &'ast Lifetime) {
        if self.unused_lifetimes.contains(&lt.ident) {
            self.found = true;
        }
        visit::visit_lifetime(self, lt);
    }
}

// Rust, why the fuck don’t you have IATs yet?
/// A wrapper around a type implementing [ToTokens] that replaces one identifier with another.
///
/// This type and the related [SubstN] and [Subst2] are used to replace type
/// parameters in generics.
pub struct Subst<'a, T: ToTokens>(pub &'a T, pub &'a Ident, pub &'a Ident);

impl<'a, T: ToTokens> ToTokens for Subst<'a, T> {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        for tt in self.0.to_token_stream() {
            tokens.append(match tt {
                TokenTree::Ident(id) if id == *(self.1) => TokenTree::Ident(self.2.clone()),
                s => s,
            });
        }
    }
}

/// A wrapper around a type implementing [ToTokens] that replaces one identifier with an arbitrary [ToTokens] object.
pub struct SubstN<'a, T: ToTokens, U: ToTokens>(pub &'a T, pub &'a Ident, pub U);

impl<'a, T: ToTokens, U: ToTokens> ToTokens for SubstN<'a, T, U> {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        for tt in self.0.to_token_stream() {
            match tt {
                TokenTree::Ident(id) if id == *(self.1) => self.2.to_tokens(tokens),
                s => tokens.append(s),
            };
        }
    }
}

/// Like [SubstN], but performs two substitutions.
pub struct Subst2<'a, T: ToTokens, U: ToTokens, V: ToTokens>(
    pub &'a T,
    pub &'a Ident,
    pub &'a U,
    pub &'a Ident,
    pub V,
);

impl<'a, T: ToTokens, U: ToTokens, V: ToTokens> ToTokens for Subst2<'a, T, U, V> {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        for tt in self.0.to_token_stream() {
            match tt {
                TokenTree::Ident(id) if id == *(self.1) => self.2.to_tokens(tokens),
                TokenTree::Ident(id) if id == *(self.3) => self.4.to_tokens(tokens),
                s => tokens.append(s),
            };
        }
    }
}
