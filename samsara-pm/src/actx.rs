use crate::{
    ctx::FsmContext,
    def::{FsmDefinition, FsmTransition},
};
use proc_macro2::TokenStream;
use quote::quote;
use syn::{Ident, Type, TypeGenerics};

#[derive(Debug)]
#[non_exhaustive]
pub struct ActionContextEnd<'a> {
    pub payload: &'a Type,
    pub cur_state: &'a Ident,
    pub prev_state: &'a Ident,
    pub prev_state_id: usize,
    pub typarams: &'a TypeGenerics<'a>,
    pub full_typarams: &'a TypeGenerics<'a>,
    pub state_trait_x: &'a TokenStream,
}

#[derive(Debug)]
#[non_exhaustive]
pub struct ActionContextStart<'a> {
    pub payload: &'a Type,
    pub cur_state: &'a Ident,
    pub next_state: &'a Ident,
    pub next_state_id: usize,
    pub typarams: &'a TypeGenerics<'a>,
    pub full_typarams: &'a TypeGenerics<'a>,
    pub state_trait_x: &'a TokenStream,
}

pub fn end_items<F>(
    transitions_ending: &[usize],
    cur_state: &Ident,
    typarams: &TypeGenerics,
    full_typarams: &TypeGenerics,
    state_trait_x: &TokenStream,
    def: &FsmDefinition,
    callback: F,
) -> (TokenStream, TokenStream)
where
    F: Fn(ActionContextEnd) -> TokenStream,
{
    let make_ctx = |j: usize| {
        let FsmTransition { start, payload, .. } = &def.transitions[j];
        let prev_state = &def.states[*start];
        ActionContextEnd {
            payload,
            cur_state,
            prev_state: &prev_state.name,
            prev_state_id: *start,
            typarams,
            full_typarams,
            state_trait_x,
        }
    };
    let clauses = transitions_ending.iter().map(|j| {
        let ctx = make_ctx(*j);
        let action = callback(ctx);
        let j = *j as u8;
        quote! {
            #j => #action,
        }
    });
    let ind_action = match transitions_ending {
        [j] => {
            let ctx = make_ctx(*j);
            callback(ctx)
        }
        _ => {
            quote! { unreachable!() }
        }
    };
    (clauses.collect(), ind_action)
}

pub fn start_items<F>(
    transitions_beginning: &[usize],
    cur_state: &Ident,
    typarams: &TypeGenerics,
    full_typarams: &TypeGenerics,
    state_trait_x: &TokenStream,
    def: &FsmDefinition,
    callback: F,
) -> (TokenStream, TokenStream)
where
    F: Fn(ActionContextStart) -> TokenStream,
{
    let make_ctx = |j: usize| {
        let FsmTransition { payload, end, .. } = &def.transitions[j];
        let next_state = &def.states[*end];
        ActionContextStart {
            payload,
            cur_state,
            next_state: &next_state.name,
            next_state_id: *end,
            typarams,
            full_typarams,
            state_trait_x,
        }
    };
    let clauses = transitions_beginning.iter().map(|j| {
        let ctx = make_ctx(*j);
        let action = callback(ctx);
        let j = *j as u8;
        quote! {
            #j => #action
        }
    });
    let ind_action = match transitions_beginning {
        [j] => {
            let ctx = make_ctx(*j);
            callback(ctx)
        }
        _ => {
            quote! { unreachable!() }
        }
    };
    (clauses.collect(), ind_action)
}

impl<'ctx> FsmContext<'ctx> {
    pub fn end_items<F>(&self, i: usize, callback: F) -> (TokenStream, TokenStream)
    where
        F: Fn(ActionContextEnd) -> TokenStream,
    {
        end_items(
            &self.transitions_ending_at[i],
            &self.def.states[i].name,
            &self.def.generics.split_for_impl().1,
            &self.modified_generics.owned_gen.split_for_impl().1,
            &self.state_trait_dx,
            &self.def,
            callback,
        )
    }
    pub fn start_items<F>(&self, i: usize, callback: F) -> (TokenStream, TokenStream)
    where
        F: Fn(ActionContextStart) -> TokenStream,
    {
        start_items(
            &self.transitions_beginning_at[i],
            &self.def.states[i].name,
            &self.def.generics.split_for_impl().1,
            &self.modified_generics.owned_gen.split_for_impl().1,
            &self.state_trait_dx,
            &self.def,
            callback,
        )
    }
}
