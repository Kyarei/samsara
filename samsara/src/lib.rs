//! Takes in a finite state machine definition and generates types for paths
//! between any two of its states.

#![feature(allocator_api)]

use std::{
    alloc::{AllocError, Allocator, Global, Layout},
    mem::MaybeUninit,
    ptr::NonNull,
};

/// The raison d’être of this crate.
///
/// Given a definition of a finite state machine, generates types that describe
/// paths between any two of its states.
///
/// This macro currently requires nightly Rust with the
/// `generic_associated_types` feature enabled. In addition, generic path types
/// also require the `generic_const_exprs` feature.
///
/// # Attributes
///
/// These are placed before the rest of the contents.
///
/// * `#[samsara_derive(...)]`: derive common traits. Currently supported:
///   [::std::clone::Clone], [::std::fmt::Debug], [::std::cmp::PartialEq].
///   [::std::cmp::Eq] is accepted but not yet implemented.
/// * `#[samsara(bidirectional)]`: derive types for the FSM with reverse
///   transitions, and methods to reverse paths.
///
/// # Example
///
/// ```
/// #![feature(generic_associated_types)]
///
/// # use samsara::samsara;
///
/// pub struct Alarm(u32);
/// pub struct Time(f64);
///
/// pub enum Food {
///     Fruit,
///     Bread,
///     Meat,
///     Carrot,
/// }
///
/// pub struct Meal {
///     pub kind: Food,
///     pub amount: f64,
/// }
///
/// samsara! {
///     pub Creature =
///     Sleeping,
///     Sitting,
///     Eating,
///     Dead,
///     Sleeping -> Alarm -> Sitting,
///     Sitting -> Meal -> Eating,
///     Eating -> Time -> Sitting,
///     Sitting -> () -> Sleeping,
///     Sitting -> () -> Dead,
///     Sleeping -> () -> Dead,
/// }
///
/// let path: CreaturePathBuf<Sleeping, Dead> =
///     CreaturePathBuf::<Sleeping, Sleeping>::new()
///         .pushed::<Sitting, _>(Alarm(34))
///         .pushed::<Eating, _>(Meal { kind: Food::Carrot, amount: 3.4 })
///         .pushed::<Sitting, _>(Time(99.4))
///         .pushed::<Dead, _>(());
/// let path_view: CreaturePath<_, _> = path.view();
/// ```
pub use samsara_pm::samsara;

/// Creates a `PathBuf` for a type defined by [samsara!].
///
/// Note that transitions are marked with `=>` instead of `->` due to macro limitations.
/// Also, all relevant state types must be in scope.
///
/// ```
/// #![feature(generic_associated_types)]
///
/// # use samsara::{samsara, make_path};
///
/// samsara! {
///     pub Spin =
///     Start,
///     A,
///     B,
///     C,
///     D,
///     End,
///     Start -> u32 -> A,
///     A -> u32 -> B,
///     B -> u32 -> C,
///     C -> u32 -> D,
///     D -> u32 -> A,
///     C -> u32 -> End,
/// }
///
/// let path = make_path![
///     SpinPathBuf: Start => 4 => A => 2 => B => 1 => C => 0 => End
/// ];
/// assert!(matches!(
///     path.view().split_last(),
///     Some(EndSplitLast::C(_, 0))
/// ));
/// ```
pub use samsara_pm::make_path;

const fn macks_aux(elems: &[usize], defo: usize) -> usize {
    match elems {
        [] => defo,
        [first, rest @ ..] => macks_aux(rest, if *first > defo { *first } else { defo }),
    }
}

/// Gets the maximum of all elements in a slice, except that the result is always at least 1.
///
/// This function is used internally by the [samsara] macro.
pub const fn macks(elems: &[usize]) -> usize {
    macks_aux(elems, 1)
}

const fn myn_aux(elems: &[usize], defo: usize) -> usize {
    match elems {
        [] => defo,
        [first, rest @ ..] => myn_aux(rest, if *first < defo { *first } else { defo }),
    }
}

/// Gets the minimum of all elements in a slice.
///
/// This function is used internally by the [samsara] macro.
pub const fn myn(elems: &[usize]) -> usize {
    myn_aux(elems, usize::MAX)
}

/// Works like [usize::next_multiple_of] but without needing nightly features.
///
/// This function is used internally by the [samsara] macro.
pub const fn next_multiple_of(a: usize, b: usize) -> usize {
    b * ((a + (b - 1)) / b)
}

const fn are_same_aux(first: usize, rest: &[usize]) -> bool {
    match rest {
        [] => true,
        [second, rest2 @ ..] => first == *second && are_same_aux(first, rest2),
    }
}

/// Returns true if all elements in the slice are equal.
///
/// This function is used internally by the [samsara] macro.
pub const fn are_same(elems: &[usize]) -> bool {
    match elems {
        [] => true,
        [first, rest @ ..] => are_same_aux(*first, rest),
    }
}

/// A wrapper around an allocator to make it allocate at a certain alignment.
pub struct AlignedAllocator<A: Allocator, const N: usize>(A);

impl<A: Allocator, const N: usize> AlignedAllocator<A, N> {
    fn align(layout: Layout) -> Layout {
        layout.align_to(N).expect("invalid value for alignment")
    }
}

unsafe impl<A: Allocator, const N: usize> Allocator for AlignedAllocator<A, N> {
    fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        self.0.allocate(Self::align(layout))
    }

    unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout) {
        self.0.deallocate(ptr, Self::align(layout))
    }

    fn allocate_zeroed(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        self.0.allocate_zeroed(Self::align(layout))
    }

    unsafe fn grow(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        self.0
            .grow(ptr, Self::align(old_layout), Self::align(new_layout))
    }

    unsafe fn grow_zeroed(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        self.0
            .grow_zeroed(ptr, Self::align(old_layout), Self::align(new_layout))
    }

    unsafe fn shrink(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        self.0
            .shrink(ptr, Self::align(old_layout), Self::align(new_layout))
    }
}

/// A vector type of bytes that ensures that memory is allocated at a given alignment.
pub type Buffer<const N: usize> = Vec<MaybeUninit<u8>, AlignedAllocator<Global, N>>;

/// Creates an empty [Buffer].
pub const fn create_buffer<const N: usize>() -> Buffer<N> {
    Buffer::new_in(AlignedAllocator(Global))
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_macks() {
        assert_eq!(macks(&[7, 2, 4]), 7);
        assert_eq!(macks(&[]), 1);
    }
    #[test]
    fn test_myn() {
        assert_eq!(myn(&[7, 2, 4]), 2);
        assert_eq!(myn(&[]), usize::MAX);
    }
    #[test]
    fn test_are_same() {
        assert!(are_same(&[]));
        assert!(are_same(&[6]));
        assert!(are_same(&[6, 6, 6, 6]));
        assert!(!are_same(&[6, 1, 6, 6]));
    }
}
