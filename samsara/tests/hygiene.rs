#![feature(generic_associated_types)]

use samsara::{make_path, samsara};

samsara! {
    #[samsara_derive(Clone, Debug, PartialEq, Eq)]
    pub EsEf =
    S,
    F,
    S -> u32 -> F,
    F -> u32 -> S,
}

#[test]
fn test() {
    let path: EsEfPathBuf<S, S> = make_path![
        EsEfPathBuf: S => 3 => F => 9 => S => 2 => F => 4 => S
    ];
    match path.view().split_last() {
        Some(SSplitLast::F(_init, payload)) => {
            assert_eq!(*payload, 4);
        }
        None => unreachable!("should not be empty"),
        Some(_) => unreachable!("last payload was not F -> u32 -> S"),
    }
}
