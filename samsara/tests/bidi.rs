#![feature(generic_associated_types)]

use samsara::{make_path, samsara};

samsara! {
    #[samsara_derive(Clone, Debug, PartialEq, Eq)]
    #[samsara(bidirectional)]
    pub ZeroOne =
    Zero,
    One,
    Zero -> u8 -> One,
    One -> bool -> Zero,
}

#[test]
fn test() {
    let fwd = make_path!(ZeroOnePathBuf: Zero => 8 => One => false => Zero => 5 => One);
    let rev = make_path!(ZeroOneRevPathBuf: One => 5 => Zero => false => One => 8 => Zero);
    assert_eq!(fwd.clone().reversed(), rev);
    assert_eq!(rev.clone().reversed(), fwd);
}

#[test]
fn test_clone() {
    let part1 = make_path!(ZeroOnePathBuf: Zero => 8 => One => false => Zero => 3 => One);
    let part2 = make_path!(ZeroOneRevPathBuf: Zero => true => One => 69 => Zero => false => One);
    let whole = part1.extended_reversed_from_slice(&part2.view());
    assert_eq!(
        whole,
        make_path!(
            ZeroOnePathBuf:
            Zero => 8 => One => false => Zero => 3 => One => false => Zero => 69 => One => true => Zero
        )
    );
}
