#![feature(generic_associated_types)]
#![feature(more_qualified_paths)]

use samsara::samsara;

samsara! {
    #[samsara_derive(Clone, Debug, PartialEq, Eq)]
    Spin =
    Start,
    A,
    B,
    C,
    D,
    End,
    Start -> u32 -> A,
    A -> u32 -> B,
    B -> u32 -> C,
    C -> u32 -> D,
    D -> u32 -> A,
    C -> u32 -> End,
}
