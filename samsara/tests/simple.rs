#![feature(generic_associated_types)]

use samsara::samsara;

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub struct Alarm(u8);

#[derive(PartialEq, Debug, Copy, Clone)]
pub struct Time(f64);

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub enum Food {
    Fruit,
    Bread,
    Meat,
    Carrot,
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub struct Meal {
    pub kind: Food,
    pub amount: f64,
}

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub struct Death {
    pub cause: &'static str,
}

samsara! {
    pub Creature =
    /// The creature is sleeping.
    Sleeping,
    /// Now it is sitting.
    Sitting,
    /// Time for a meal!
    Eating,
    /// Rest in peace.
    Dead,
    /// Wake up the creature with an alarm.
    Sleeping -> Alarm -> Sitting,
    /// Give the creature a meal.
    Sitting -> Meal -> Eating,
    /// Wait until the creature is done eating.
    Eating -> Time -> Sitting,
    /// Let the creature go to sleep.
    Sitting -> () -> Sleeping,
    /// Put the creature to sleep.
    Sitting -> Death -> Dead,
    /// Put the creature to sleep.
    Sleeping -> Death -> Dead,
}

#[test]
fn test() {
    let path: CreaturePathBuf<Sleeping, Dead> = CreaturePathBuf::<Sleeping, Sleeping>::new()
        .pushed::<Sitting, _>(Alarm(34))
        .pushed::<Eating, _>(Meal {
            kind: Food::Carrot,
            amount: 3.4,
        })
        .pushed::<Sitting, _>(Time(99.4))
        .pushed::<Dead, _>(Death { cause: "old age" });
    let mut path = match path.popped() {
        Err(_) => panic!("path was empty!"),
        Ok(DeadPop::Sitting(path, e)) => {
            assert_eq!(e, Death { cause: "old age" });
            path
        }
        _ => panic!("prior state was not Sitting"),
    };
    let path_view: CreaturePath<_, _> = path.view();
    match path_view.split_first() {
        None => panic!("expected Some for split_first"),
        Some(SleepingSplitFirst::Sitting(alarm, _)) => assert_eq!(*alarm, Alarm(34)),
        Some(_) => panic!("prior state was not Eating"),
    }
    match path_view.split_last() {
        None => panic!("expected Some for split_last"),
        Some(SittingSplitLast::Eating(_, time)) => assert_eq!(*time, Time(99.4)),
        Some(_) => panic!("prior state was not Eating"),
    }
    // TODO: make the API less awkward to use
    let mut path_view_mut: CreaturePathMut<_, _> = path.view_mut();
    match path_view_mut.split_first_mut() {
        None => panic!("expected Some for split_first_mut"),
        Some(SleepingSplitFirstMut::Sitting(alarm, _)) => alarm.0 += 4,
        Some(_) => panic!("prior state was not Eating"),
    }
    match path_view_mut.split_last_mut() {
        None => panic!("expected Some for split_last_mut"),
        Some(SittingSplitLastMut::Eating(_, time)) => time.0 /= 2.0,
        Some(_) => panic!("prior state was not Eating"),
    }
    match path_view_mut.split_first() {
        None => panic!("expected Some for split_first"),
        Some(SleepingSplitFirst::Sitting(alarm, _)) => assert_eq!(*alarm, Alarm(38)),
        Some(_) => panic!("prior state was not Eating"),
    }
    match path_view_mut.split_last() {
        None => panic!("expected Some for split_last"),
        Some(SittingSplitLast::Eating(_, time)) => assert_eq!(*time, Time(49.7)),
        Some(_) => panic!("prior state was not Eating"),
    }
}
