#![feature(generic_associated_types)]
#![feature(generic_const_exprs)]

use std::fmt::Display;

use samsara::{make_path, samsara};

pub trait Eat {
    fn eat(self) -> String; // returns a message
}

impl Eat for u32 {
    fn eat(self) -> String {
        match self {
            0 => "".into(),
            n => {
                let mut s = "Nom".into();
                for _ in 1..n {
                    s += " nom";
                }
                s += "!";
                s
            }
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
enum Cookie {
    ChocolateChip,
    OatmealRaisin,
}

impl Eat for Cookie {
    fn eat(self) -> String {
        (match self {
            Cookie::ChocolateChip => "I love chocolate!",
            Cookie::OatmealRaisin => "It’s just okay.",
        })
        .into()
    }
}

samsara! {
    #[samsara_derive(Clone, PartialEq, Eq, Debug)]
    pub ZeroOneTwo<A, B: Display> where A: Eat =
    Zero,
    One,
    Two,
    Zero -> A -> One,
    One -> B -> Two,
    Zero -> (A, B) -> Two,
    Two -> (B, A) -> Zero,
}

#[test]
fn test_with_cookies() {
    let cookie_path = make_path![
        ZeroOneTwoPathBuf<Cookie, &str>:
        Zero =>
            Cookie::ChocolateChip => One
            => "opo" => Two =>
            ("pop", Cookie::OatmealRaisin) => Zero
    ];
    match cookie_path.view().split_first() {
        Some(ZeroSplitFirst::One(cookie, _next)) => assert_eq!(*cookie, Cookie::ChocolateChip),
        None => unreachable!("should not be empty"),
        Some(_) => unreachable!("first payload was not Zero -> A -> One"),
    }
}

#[test]
fn test_with_u32s() {
    let u32_path = make_path![
        ZeroOneTwoPathBuf<u32, f64>:
        Zero =>
            3u32 => One
            => 6.2f64 => Two =>
            (0.9f64, 5u32) => Zero
    ];
    match u32_path.view().split_last() {
        Some(ZeroSplitLast::Two(_prev, (num, fodder))) => {
            assert!(*num < 2.0);
            assert_eq!(fodder.eat(), "Nom nom nom nom nom!");
        }
        None => unreachable!("should not be empty"),
        Some(_) => unreachable!("last payload was not Two -> (B, A) -> Zero"),
    }
}
