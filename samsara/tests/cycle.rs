#![feature(generic_associated_types)]
#![feature(more_qualified_paths)]

use samsara::{make_path, samsara};

samsara! {
    #[samsara_derive(Clone, Debug, PartialEq, Eq)]
    pub Spin =
    Start,
    A,
    B,
    C,
    D,
    End,
    Start -> u32 -> A,
    A -> u32 -> B,
    B -> u32 -> C,
    C -> u32 -> D,
    D -> u32 -> A,
    C -> u32 -> End,
}

macro_rules! def_append_path {
    ($fn_name:ident, $start:ident $(, $succ:ident, $call_name:ident)*) => {
        fn $fn_name(path: SpinPath<$start, End>, out: &mut Vec<u32>) {
            match path.split_first() {
                None => (),
                $(Some(<$start as SpinState>::SplitFirst::$succ(val, rest)) => {
                    out.push(*val);
                    $call_name(rest, out);
                },)*
            }
        }
    }
}
def_append_path!(push_to_path, Start, A, push_to_path_from_a);
def_append_path!(push_to_path_from_a, A, B, push_to_path_from_b);
def_append_path!(push_to_path_from_b, B, C, push_to_path_from_c);
def_append_path!(push_to_path_from_c, C, D, push_to_path_from_d, End, done);
def_append_path!(push_to_path_from_d, D, A, push_to_path_from_a);
fn done(_path: SpinPath<End, End>, _out: &mut Vec<u32>) {}

fn get_path(path: SpinPath<Start, End>) -> Vec<u32> {
    let mut v = Vec::new();
    push_to_path(path, &mut v);
    v
}

#[test]
fn test() {
    let path: SpinPathBuf<Start, End> = SpinPathBuf::<Start, Start>::new()
        .pushed::<A, _>(1)
        .pushed::<B, _>(4)
        .pushed::<C, _>(6)
        .pushed::<D, _>(2)
        .pushed::<A, _>(8)
        .pushed::<B, _>(3)
        .pushed::<C, _>(7)
        .pushed::<End, _>(0);
    assert_eq!(get_path(path.view()), vec![1, 4, 6, 2, 8, 3, 7, 0]);
    let mut path2 = path.clone();
    let EndPop::C(path, trans) = path.popped().expect("end of path");
    assert_eq!(trans, 0);
    let path = path.pushed::<End, _>(9);
    assert_eq!(get_path(path.view()), vec![1, 4, 6, 2, 8, 3, 7, 9]);
    assert_eq!(get_path(path2.view()), vec![1, 4, 6, 2, 8, 3, 7, 0]);
    let mut path3 = make_path! {
        SpinPathBuf: Start => 1 => A => 4 => B => 6 => C => 2 => D => 8 => A => 3 => B => 7 => C => 0 => End
    };
    assert_eq!(path2.view(), path3.view());
    assert_ne!(path.view(), path3.view());
    assert_eq!(path2, path3);
    assert_eq!(path2.view_mut(), path3.view_mut());
    assert_eq!(format!("{:?}", path), "SpinPathBuf { Start => 1 => A => 4 => B => 6 => C => 2 => D => 8 => A => 3 => B => 7 => C => 9 => End }");
}
