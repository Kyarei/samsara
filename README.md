# samsara

A proc macro that takes a definition of a finite state machine and generates types referring to paths through it.

Currently works only on nightly Rust due to use of GATs.

## Example

```rust
#![feature(generic_associated_types)] // always needed
#![feature(generic_const_exprs)] // needed only when defining generic FSMs

use std::fmt::Display;

use samsara::{make_path, samsara};

pub trait Eat {
    fn eat(self) -> String; // returns a message
}

impl Eat for u32 {
    fn eat(self) -> String {
        match self {
            0 => "".into(),
            n => {
                let mut s = "Nom".into();
                for _ in 1..n {
                    s += " nom";
                }
                s += "!";
                s
            }
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
enum Cookie {
    ChocolateChip,
    OatmealRaisin,
}

impl Eat for Cookie {
    fn eat(self) -> String {
        (match self {
            Cookie::ChocolateChip => "I love chocolate!",
            Cookie::OatmealRaisin => "It’s just okay.",
        })
        .into()
    }
}

samsara! {
    // Derive some traits
    #[samsara_derive(Clone, PartialEq, Eq, Debug)]
    pub ZeroOneTwo<A, B: Display> where A: Eat =
    // Define the states
    Zero,
    One,
    Two,
    // Define transitions (with their payloads)
    Zero -> A -> One,
    One -> B -> Two,
    Zero -> (A, B) -> Two,
    Two -> (B, A) -> Zero,
}

// This defines a type for each state: Zero, One, and Two, with two type parameters each.
// Also defined are traits for states (ZeroOneTwoState) and payloads (ZeroOneTwoPayload).
// A container type (ZeroOneTwoPathBuf), as well as view types (ZeroOneTwoPath and
// ZeroOneTwoPathMut), is defined as well.

#[test]
fn test_with_cookies() {
    let cookie_path = make_path![
        ZeroOneTwoPathBuf<Cookie, &str>:
        Zero =>
            Cookie::ChocolateChip => One
            => "opo" => Two =>
            ("pop", Cookie::OatmealRaisin) => Zero
    ];
    match cookie_path.view().split_first() {
        Some(ZeroSplitFirst::One(cookie, _next)) => assert_eq!(*cookie, Cookie::ChocolateChip),
        None => unreachable!("should not be empty"),
        Some(_) => unreachable!("first payload was not Zero -> A -> One"),
    }
}

#[test]
fn test_with_u32s() {
    let u32_path = make_path![
        ZeroOneTwoPathBuf<u32, f64>:
        Zero =>
            3u32 => One
            => 6.2f64 => Two =>
            (0.9f64, 5u32) => Zero
    ];
    match u32_path.view().split_last() {
        Some(ZeroSplitLast::Two(_prev, (num, fodder))) => {
            assert!(*num < 2.0);
            assert_eq!(fodder.eat(), "Nom nom nom nom nom!");
        }
        None => unreachable!("should not be empty"),
        Some(_) => unreachable!("last payload was not Two -> (B, A) -> Zero"),
    }
}
```

## TODO

This crate was originally designed for [f9i](https://gitlab.com/Kyarei/f9i), but I’ve decided to move it to its own repository.

Enhancements that this crate needs:

* more testing
* derive `Eq` when asked
* customize container types for owning path type (currently hardcoded to `Vec`, but I’d like to be able to use it with [`SmallVec`](https://github.com/servo/rust-smallvec) as well, although it currently does not support custom allocators)
* let more things be const (but this needs many, *many* const-related features that Rust doesn’t have yet and is unlikely to have for a long time)
* improve ergonomics, especially with iteration
* ~~add support for generating reverse version of FSM~~ (preliminary support done)
* refactor out things from The One Big Function™
